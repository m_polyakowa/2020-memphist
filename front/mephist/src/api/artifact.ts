import { rest } from '../utils/rest';

export interface ArtifactInputParams {
  name: string;
  professor: string;
  subject: number;
  artifact: string;
}

export interface ArtifactOutputParams {
  id: number;
  name: string;
  artifact: string;
  date_created: Date;
  file_hash: string;
  professor: number;
  subject: number;
}

export interface ArtifactParams {
  id: number;
  name: string;
  artifact: string;
  date_created: Date;
  file_hash: string;
  professor: number;
  subject: number;
}

export const uploadArtifact = (params: any) => rest.post<ArtifactOutputParams>('/subject_artifacts/upload',
  params,
  {
    headers: {
      'content-type': 'multipart/form-data',
    },
  });

export const downloadArtifact = (id: number) => rest.get<Blob>(`/subject_artifacts/${id}`, { responseType: 'blob' });

export const getArtefact = (slug: string, subject_id: number) => rest.get<ArtifactParams[]>(`/professors/${slug}/${subject_id}`);

export const deleteArtefact = (id: number) => rest.delete(`/subject_artifacts/${id}`);
