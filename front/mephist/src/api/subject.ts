import { rest } from '../utils/rest';

export interface SubjectParams {
  id: number;
  name: string;
  description: string | null;
}

export interface SubjectErrorParams {
  id?: number;
  name?: string;
  description?: string | null;
}

export const getSubjectList = () => rest.get<SubjectParams[]>('/subjects/');

export const getSubject = (num: number) => rest.get<SubjectParams>(`/subjects/${num}`);

export const addSubject = (params: {
  name: string,
  description: string
}) => rest.post<SubjectParams>('/subjects/add_subject', params);
