import { rest } from '../utils/rest';

export interface RatingParams {
  user_type: 'ch' | 'qu' | 'ex';
  value: number;
  content_id: number;
}

export interface UserRatingParams {
  user: number;
  user_details: {
      username: string;
      email: string;
  };
  content_type: number;
  content_id: number;
  rating_type: string;
  value: number;
}

export const getUserProfessorRating = (userType: string, id: number) => rest.get<UserRatingParams>(`/rating/user/${userType}/${id}`);

export const updateProfessorRating = (params: RatingParams) => rest.post('/rating/add_rating', params);
