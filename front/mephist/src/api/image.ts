import { rest } from '../utils/rest';

export interface uploadImageParams {
    image: string;
}

export interface ImageParams {
    id: number;
    image: string;
    date_created: Date;
    file_hash: string;
}

export const uploadImage = (params: any) => rest.post<ImageParams>('/images/upload',
  params,
  {
    headers: {
      'content-type': 'multipart/form-data',
    },
  });

export const deleteImage = (id: number) => rest.delete(`/images/${id}`);
