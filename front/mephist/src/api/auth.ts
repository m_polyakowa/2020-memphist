import { AxiosRequestConfig, rest } from '../utils/rest';
import { User } from '../components/auth/AuthProvider';
import { BackValidationErr } from '../types';

interface AuthRequest {
  email: string;
  password: string;
}

type AuthRes = User;

export type AuthResError = BackValidationErr<AuthRequest>

interface SignUpRequest {
  email: string;
  username: string;
  password: string;
}

export type SignUpResError = BackValidationErr<SignUpRequest>;

export interface ActivateRequest {
  uidb64: string;
  token: string;
}

export const auth = async (params: AuthRequest, config?: AxiosRequestConfig) => rest.post<AuthRes, AuthResError>('/account/login', params, config);

export const signUp = async (params: SignUpRequest, config?: AxiosRequestConfig) => rest.post<'', SignUpResError>('/account/signup', params, config);

export const activate = async (params: ActivateRequest, config?: AxiosRequestConfig) => rest.get(`/account/activate/${params.uidb64}/${params.token}`, config);

export const logout = async (config?: AxiosRequestConfig) => rest.get('/account/logout', config);

export const init = async (config?: AxiosRequestConfig) => rest.get<User>('/account/getdata', config);
