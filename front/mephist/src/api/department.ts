import { rest } from '../utils/rest';

export interface DepartmentParams {
    id: number;
    number: number;
    name: string;
    is_active: boolean;
}

export interface CreateDepartmentParams {
  number: string;
  name: string;
  is_active: true;
}

export const getDepartmentsList = () => rest.get<DepartmentParams[]>('/departments/');

export const addDepartment = (params: CreateDepartmentParams) => rest.post<DepartmentParams>('/departments/add_department', params);

export const getDepartment = (num: number) => rest.get<DepartmentParams>(`/departments/${num}`);
