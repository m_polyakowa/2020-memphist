import { rest } from '../utils/rest';
import { DepartmentParams } from './department';
import { SubjectParams } from './subject';

export type ProfessorParams = {
  fullname: string;
  departments: number[];
  subjects: number[];
  birthday?: Date | null;
}

export type AdditionalProfessorParams = {
  id: number;
  slug: string;
  department_details: DepartmentParams[];
  subject_details: SubjectParams[];
  image?: number;
  image_url?: string;
  rating_ch_avg?: number;
  rating_qu_avg?: number;
  rating_ex_avg?: number;
  rating_ch_num?: number;
  rating_qu_num?: number;
  rating_ex_num?: number;
}

export type ExtendedProfessorParams = ProfessorParams & AdditionalProfessorParams;

export interface ProfessorListElementParams {
  slug: string;
  fullname: string;
}

export interface ProfessorsListParams {
  num: number;
  lastId: string;
}

export interface Feedback {
  id: number;
  username: string;
  title: string;
  datetime: string;
  content: string;
}

export interface AddFeedbackRequest {
  slug: string;
  title: string;
  content: string;
}

export interface UpdateFeedbackRequest {
  title: string;
  content: string;
}

export interface RatingParams {
  user_type: 'ch' | 'qu' | 'ex';
  value: number;
  content_id: number;
}

export interface TopParams {
  slug: string;
  fullname: string;
  avg_rating: number;
}

export const createProfessor = (params: ProfessorParams) => rest.post('/professors/add_professor', params);

// todo: пагинация
export const getProfessorsList = () => rest.get<ProfessorListElementParams[]>('/professors/');

export const getProfessorDetails = (slug: string) => rest.get<ExtendedProfessorParams>(`/professors/${slug}`);

export const deleteProfessor = (slug: string) => rest.delete(`/professors/${slug}`);

export const editProfessor = (slug: string, params: ProfessorParams) => rest.put<ExtendedProfessorParams>(`/professors/${slug}`, params);

export const getProfessorFeedback = (slug: string) => rest.get<Feedback[]>(`/professors/${slug}/list_feedbacks`);

export const addProfessorFeedback = (params: AddFeedbackRequest) => rest.post<Feedback>('/professors/add_feedback', params);

export const deleteProfessorFeedback = (id: number) => rest.delete(`/professors/feedback/${id}`);

export const updateProfessorFeedback = (id: number, params: UpdateFeedbackRequest) => rest.patch<Feedback>(`/professors/feedback/${id}`, params);

export const updateProfessorRating = (params: RatingParams) => rest.post('/rating/add_rating', params);

export const getTopBestProfessors = () => rest.get<TopParams[]>('/professors/top_best_professors');

export const getTopWorstProfessors = () => rest.get<TopParams[]>('/professors/top_worst_professors');
