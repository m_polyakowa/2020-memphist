import React, { ReactElement, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory, Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import MenuIcon from '@material-ui/icons/Menu';
import Popover from '@material-ui/core/Popover';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { useAuth } from '../components/auth/AuthProvider';
import { logout } from '../api/auth';
import ProfessorsList from '../components/ProfessorList';
import MESSAGES from '../store/messages';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  link: {
    color: 'white',
    marginRight: 10,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  buttonLink: {
    textDecoration: 'none',
    textTransform: 'none',
  },
}));

interface Props {
  children: ReactElement | ReactElement[];
}

function ResponsiveDrawer({ children }: Props) {
  const classes = useStyles();
  const theme = useTheme();
  const { user, signout } = useAuth();
  const { t } = useTranslation();
  const history = useHistory();

  const initPopoverContent: ReactElement = <div />;

  const [mobileOpen, setMobileOpen] = useState(false);
  const [popoverContent, setPopoverContent] = useState(initPopoverContent);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const isStaff = user?.is_staff;

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
    if (mobileOpen) {
      setAnchorEl(null);
    }
  };

  const logoutUser = async () => {
    const res = await logout();
    if (res.state === 'success') {
      signout(() => {});
    }
  };

  const onDrawerButtonClick = () => {
    ProfessorsList().then((res) => {
      setPopoverContent(res);
      setAnchorEl(window.document.body);
    });
  };

  const onCreateProfessorClick = () => {
    history.push('/create-professor');
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? 'simple-popover' : undefined;

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        <ListItem button key="professors" onClick={onDrawerButtonClick}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary={t(MESSAGES.PROFESSORS)} />
        </ListItem>
        {isStaff && (
          <ListItem button key="create-professor" onClick={onCreateProfessorClick}>
            <ListItemIcon>
              <MailIcon />
            </ListItemIcon>
            <ListItemText primary={t(MESSAGES.CREATE_PROFESSOR)} />
          </ListItem>
        )}
      </List>
      <Divider />
      <List>
        {['Перлы'].map((text, index) => (
          <ListItem button key={text}>
            <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
          >
            <MenuIcon />
          </IconButton>
          {
            user
              // eslint-disable-next-line jsx-a11y/anchor-is-valid
              ? <Link className={classes.link} to="#" onClick={logoutUser}>Выйти</Link>
              : (
                <>
                  <Link to="login" className={classes.link}>
                    Войти
                  </Link>
                  <Link to="sign-up" className={classes.link}>
                    Зарегистрироваться
                  </Link>
                </>
              )
          }
        </Toolbar>
      </AppBar>
      <nav aria-label="mailbox folders">
        <Drawer
          variant="temporary"
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={mobileOpen}
          onClose={handleDrawerToggle}
          classes={{
            paper: classes.drawerPaper,
          }}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
        >
          {drawer}
        </Drawer>
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handlePopoverClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          {popoverContent}
        </Popover>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        { children }
      </main>
    </div>
  );
}

export default ResponsiveDrawer;
