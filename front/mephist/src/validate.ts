/* eslint-disable no-template-curly-in-string */
import * as yup from 'yup';

yup.setLocale({
  mixed: {
    required: 'Поле является обязательным для заполнения',
  },
  string: {
    email: 'Поле содержит невалидный email-адрес',
    min: 'Минимальное количество символов ${min}',
    max: 'Максимальное количество символов ${max}',
  },
  number: {
    min: 'Значение должно быть не меньше ${min}',
    max: 'Значение должно быть не больше ${max}',
  },
});

export default yup;
