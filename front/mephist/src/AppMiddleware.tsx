import React, { ReactElement, useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';
import { useAuth } from './components/auth/AuthProvider';
import { init } from './api/auth';

interface Props {
  children: ReactElement | ReactElement[];
}

type State = 'LOADING' | 'OK'

const useStyles = makeStyles({
  root: {
    height: '100vh',
  },
});

function AppMiddleware({ children }: Props) {
  const { signin } = useAuth();
  const classes = useStyles();
  const [state, setState] = useState<State>('LOADING');

  useEffect(() => {
    init()
      .then((res) => {
        if (res.state === 'success') {
          signin({ ...res.data }, () => {});
        }
        setState('OK');
      });
  }, []);

  if (state === 'LOADING') {
    return (
      <Grid
        className={classes.root}
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <CircularProgress />
      </Grid>
    );
  }

  return (
    <>
      { children }
    </>
  );
}

export default AppMiddleware;
