export type BackValidationErr<T extends object> = Partial<Record<keyof T, string[]>>
