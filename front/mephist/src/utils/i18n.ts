import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import LanguageDetector from 'i18next-browser-languagedetector';

import MESSAGES from '../store/messages';

const resources = {
  en: {
    translation: {
      [MESSAGES.REQUIRED]: 'This field is required',
      [MESSAGES.WRONG_NAME_PATTERN]: 'Wrong pattern! Please, use cyrillic symbols, first letter must be capital',
      [MESSAGES.INVALID_EMAIL]: 'Invalid email',
      [MESSAGES.SUBMIT]: 'Submit',
      [MESSAGES.FIRST_NAME]: 'First name',
      [MESSAGES.LAST_NAME]: 'Last name',
      [MESSAGES.PATRONYMIC]: 'Patronymic',
      [MESSAGES.BIRTHDAY]: 'Birthday',
      [MESSAGES.DESCRIPTION]: 'Description',
      [MESSAGES.DEPARTMENT]: 'Department',
      [MESSAGES.PROFESSORS]: 'Professors',
      [MESSAGES.CREATE_PROFESSOR]: 'Add a professor', // ????
    },
  },
};

i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    resources,
    lng: 'ru',

    interpolation: {
      escapeValue: false,
    },
    react: {
      useSuspense: false,
    },

    nsSeparator: false,
    keySeparator: false,

    fallbackLng: false,
  });

export default i18n;
