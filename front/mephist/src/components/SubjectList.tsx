import React, { useEffect, useState } from 'react';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import CloudUpload from '@material-ui/icons/CloudUpload';
import DeleteIcon from '@material-ui/icons/Delete';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Popover from '@material-ui/core/Popover';

import ArtifactsList from './ArtifactList';
import { SubjectParams } from '../api/subject';
import {
  ArtifactParams, uploadArtifact, getArtefact, deleteArtefact,
} from '../api/artifact';

interface Props {
  slug: string;
  list: SubjectParams[];
  // eslint-disable-next-line
  handleDelete: (id: number) => Promise<void>;
}

interface ElementProps extends SubjectParams {
  // eslint-disable-next-line
  handleDelete: (id: number) => Promise<void>;
  slug: string;
}

const useStyles = makeStyles(() => ({
  element: {
    display: 'flex',
    justifyContent: 'flex-start',
  },
  container: {
    width: '100%',
    marginTop: '20px',
  },
  elementContainer: {
    width: '100%',
    padding: '10px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'column',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: '20px',
  },
  popover: {
    padding: '15px',
  },
}));

const SubjectElement: React.FC<ElementProps> = (props: ElementProps) => {
  const {
    id,
    name,
    description,
    handleDelete,
    slug,
  } = props;
  const classes = useStyles();

  const [expanded, setExpanded] = useState(false);
  const [anchorDeleteEl, setAnchorDeleteEl] = useState<HTMLElement | null>(null);
  const popoverDeleteOpen = Boolean(anchorDeleteEl);
  const [anchorUploadEl, setAnchorUploadEl] = useState<HTMLElement | null>(null);
  const popoverUploadOpen = Boolean(anchorUploadEl);
  const [file, setFile] = useState<File | null>(null);
  const [isPicked, setPicked] = useState(false);
  const [artifacts, setArtifacts] = useState<ArtifactParams[]>([]);

  const getArtefacts = async () => {
    const response = await getArtefact(slug, id);
    if (response.state === 'success') {
      const { data } = response;
      setArtifacts(data || []);
    } else {
      // todo error
      console.log('error');
    }
  };

  useEffect(() => {
    getArtefacts();
  }, []);

  const onAddClick = () => {
    setAnchorUploadEl(window.document.body);
  };

  const onDeleteClick = () => {
    setAnchorDeleteEl(window.document.body);
  };

  const onExpand = () => {
    setExpanded(true);
  };

  const onCollapse = () => {
    setExpanded(false);
  };

  const handleDeletePopoverClick = () => {
    handleDelete(id);
    setAnchorDeleteEl(null);
  };

  const handleDeletePopoverClose = () => {
    setAnchorDeleteEl(null);
  };

  const handleUploadPopoverClose = () => {
    setAnchorUploadEl(null);
  };

  const uploadHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPicked(true);
    const { target } = event;
    if (target && target.files) {
      setFile(target.files[0]);
    }
  };

  const deletePopover = (
    <Popover
      id="deleteSubjectRelation"
      open={popoverDeleteOpen}
      onClose={handleDeletePopoverClose}
      anchorEl={anchorDeleteEl}
      anchorOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
    >
      <div className={classes.popover}>
        <Typography>
          Вы уверены, что хотите удалить дисциплину
          из списка преподаваемых преподавателем дисциплин?
        </Typography>
        <div className={classes.buttonContainer}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleDeletePopoverClick}
          >
            Удалить
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={handleDeletePopoverClose}
          >
            Отмена
          </Button>
        </div>
      </div>
    </Popover>
  );

  const handleUploadClick = async () => {
    if (file) {
      if (file.size < 100 * 1024 * 1024) {
        const formData = new FormData();
        formData.append('name', file.name);
        formData.append('professor', slug);
        formData.append('subject', String(id));
        formData.append('artifact', file, file.name);
        const response = await uploadArtifact(formData);
        if (response.state === 'success') {
          getArtefacts();
          setAnchorUploadEl(null);
        } else {
          // todo
        }
      } else {
        // todo
        console.log('Файл должен быть меньше 100мб');
      }
    } else {
      // todo
    }
  };

  const handleArtifactDelete = async (subjectId: number) => {
    const response = await deleteArtefact(subjectId);
    if (response.state === 'success') {
      getArtefacts();
    } else {
      // todo error
    }
  };

  const fileUploadPopover = (
    <Popover
      id="deleteSubjectRelation"
      open={popoverUploadOpen}
      onClose={handleUploadPopoverClose}
      anchorEl={anchorUploadEl}
      anchorOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
    >
      <div className={classes.popover}>
        <Typography>
          Загрузите файл
        </Typography>
        <input type="file" name="file" onChange={uploadHandler} />
        <div className={classes.buttonContainer}>
          {isPicked && (
            <Button
              variant="contained"
              color="primary"
              onClick={handleUploadClick}
            >
              Отправить
            </Button>
          )}
          <Button
            variant="contained"
            color="primary"
            onClick={handleUploadPopoverClose}
          >
            Отмена
          </Button>
        </div>
      </div>
    </Popover>
  );

  return (
    <React.Fragment key={id}>
      <ListItem alignItems="flex-start">
        <div className={classes.elementContainer}>
          <div className={classes.textContainer}>
            <Typography variant="subtitle1" className={classes.element}>{name}</Typography>
            <Typography variant="subtitle2" className={classes.element}>{description}</Typography>
          </div>
          <div>
            {expanded ? (
              <IconButton onClick={onCollapse} size="small">
                <ExpandMoreIcon />
              </IconButton>
            ) : (
              <IconButton onClick={onExpand} size="small">
                <ChevronLeftIcon />
              </IconButton>
            )}
            <IconButton onClick={onAddClick}>
              <CloudUpload />
            </IconButton>
            <IconButton onClick={onDeleteClick}>
              <DeleteIcon />
            </IconButton>
          </div>
        </div>
      </ListItem>
      <Divider />
      {expanded && (
        <ArtifactsList
          list={artifacts}
          professor={slug}
          subjectId={id}
          handleDelete={handleArtifactDelete}
        />
      )}
      {deletePopover}
      {fileUploadPopover}
    </React.Fragment>
  );
};

const SubjectsList: React.FC<Props> = (props: Props) => {
  const classes = useStyles();
  const {
    slug,
    list,
    handleDelete,
  } = props;

  return (
    <div className={classes.container}>
      {list.map((data: SubjectParams) => {
        const { id, name, description } = data;
        return (
          <SubjectElement
            key={id}
            id={id}
            name={name}
            description={description}
            handleDelete={handleDelete}
            slug={slug}
          />
        );
      })}
    </div>
  );
};

export default SubjectsList;
