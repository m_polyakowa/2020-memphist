import React, {
  useContext, createContext, useState, FC, ReactNode,
} from 'react';

// исходник https://reactrouter.com/web/example/auth-workflow

type AuthCallback = () => void;

export type User = {
  username: string;
  is_staff: boolean;
};

export type UserExtended = User | null;

const fakeAuth = {
  signin(user: UserExtended, cb: AuthCallback) {
    setTimeout(cb, 100); // fake async
  },
  signout(cb: AuthCallback) {
    setTimeout(cb, 100);
  },
};

type AuthContextType = typeof fakeAuth & {
  user: UserExtended,
};

const AuthContext = createContext<AuthContextType>({
  user: null,
  signin: (user, cb) => cb(),
  signout: (cb) => cb(),
});

function useProvideAuth() {
  const [user, setUser] = useState<UserExtended>(null);

  const signin = (userName: UserExtended, cb: AuthCallback) => fakeAuth.signin(userName, () => {
    setUser(userName);
    cb();
  });

  const signout = (cb: AuthCallback) => fakeAuth.signout(() => {
    setUser(null);
    cb();
  });

  return {
    user,
    signin,
    signout,
  };
}

const ProvideAuth: FC<{ children: ReactNode }> = ({ children }) => {
  const auth = useProvideAuth();
  return (
    <AuthContext.Provider value={auth}>
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  return useContext(AuthContext);
}

export default ProvideAuth;
