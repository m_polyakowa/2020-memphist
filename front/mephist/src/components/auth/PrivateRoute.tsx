import React, { FC, ReactChild } from 'react';
import { Route, Redirect, RouteProps } from 'react-router-dom';
import { useAuth } from './AuthProvider';

const PrivateRoute: FC<RouteProps & {
  children: ReactChild,
}> = ({ children, ...rest }) => {
  const auth = useAuth();

  return (
    <Route
      /* eslint-disable react/jsx-props-no-spreading */
      {...rest}
      render={({ location }) => (auth?.user ? (
        children
      ) : (
        <Redirect
          to={{
            pathname: '/login',
            state: { from: location },
          }}
        />
      ))}
    />
  );
};

export default PrivateRoute;
