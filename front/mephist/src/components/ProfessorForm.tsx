import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import { useForm } from 'react-hook-form';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete, { AutocompleteRenderInputParams } from '@material-ui/lab/Autocomplete';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';

import { makeStyles } from '@material-ui/core/styles';
import {
  getDepartmentsList, addDepartment, DepartmentParams,
} from '../api/department';
import { editProfessor, createProfessor, ExtendedProfessorParams } from '../api/professor';
import MESSAGES from '../store/messages';
import { LocationState } from '../store/location';
import { getSubjectList, addSubject, SubjectParams } from '../api/subject';
import { uploadImage } from '../api/image';

interface BaseProps {
  mode: 'edit' | 'create',
}

interface CreateProps extends BaseProps {
  mode: 'create',
}

interface EditProps extends BaseProps {
  mode: 'edit',
  slug: string;
  firstName: string;
  lastName: string;
  patronimic: string;
  birthday?: Date | null;
  selectedDepartments: DepartmentParams[];
  selectedSubjects: SubjectParams[];
  onEditHandle: React.Dispatch<React.SetStateAction<ExtendedProfessorParams>>;
  handlePopoverClose: () => void;
}

type Props = CreateProps | EditProps;

export interface Fields {
  lastName: string,
  firstName: string,
  patronymic?: string,
  birthday?: Date | null,
}

interface errorObject {
  type: string;
  message: string;
  ref: Element;
}

interface Messages {
  lastName?: string;
  firstName?: string;
  patronymic?: string;
}

interface Error {
  firstName?: errorObject;
  lastName?: errorObject;
  patronymic?: errorObject;
}

const useStyles = makeStyles(() => ({
  professorElement: {
    display: 'flex',
    justifyContent: 'center',
  },
  popover: {
    padding: 20,
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: '20px',
  },
}));

/* eslint-disable react/destructuring-assignment */
const ProfessorForm: React.FC<Props> = (props: Props) => {
  const { t } = useTranslation();
  const history = useHistory();
  const classes = useStyles();
  const location = useLocation<LocationState>();
  const { from } = location.state || { from: { pathname: '/' } };
  const { register, handleSubmit, errors } = useForm();
  const [departments, setDepartments] = useState<string[]>([]);
  const [subjects, setSubjects] = useState<string[]>([]);
  const [openDepartment, setOpenDepartment] = useState(false);
  const [openSubject, setOpenSubject] = useState(false);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [anchorSubjectEl, setAnchorSubjectEl] = useState<null | HTMLElement>(null);
  const [anchorErrorEl, setAnchorErrorEl] = useState<null | HTMLElement>(null);
  const [errorString, setErrorString] = useState('Что-то пошло не так');
  const [departmentName, setDepartmentName] = useState('');
  const [departmentNumber, setDepartmentNumber] = useState('');
  const [subjectName, setSubjectName] = useState('');
  const [subjectDescription, setSubjectDescription] = useState('');
  const [imageId, setImageId] = useState<null | number>(null);
  const selectedDepartment = props.mode === 'edit'
    ? props.selectedDepartments.map((val) => `${val.number}. ${val.name}`)
    : [];
  const selectedSubject = props.mode === 'edit'
    ? props.selectedSubjects.map((val) => `${val.name}`)
    : [];
  const [
    departmentAutoComplete,
    setDepartmentAutoComplete,
  ] = useState<string[]>(selectedDepartment);
  const [departmentMap, setDepartmentMap] = useState<Record<string, number>>({});
  const [subjectAutoComplete, setSubjectAutoComplete] = useState<string[]>(selectedSubject);
  const [subjectMap, setSubjectMap] = useState<Record<string, number>>({});
  const [anchorUploadEl, setAnchorUploadEl] = useState<HTMLElement | null>(null);
  const [image, setImage] = useState<File | null>(null);
  const [isPicked, setPicked] = useState(false);
  const openPopover = Boolean(anchorEl);
  const openSubjectPopover = Boolean(anchorSubjectEl);
  const openErrorPopover = Boolean(anchorErrorEl);
  const popoverUploadOpen = Boolean(anchorUploadEl);

  useEffect(() => {
    (async () => {
      const departmentResponse = await getDepartmentsList();
      if (departmentResponse.state === 'success') {
        const { data } = departmentResponse;
        if (data) {
          const tempMap: Record<string, number> = {};
          const toSet = data.map((opt) => {
            tempMap[`${opt.number}. ${opt.name}`] = opt.id;
            return `${opt.number}. ${opt.name}`;
          });
          setDepartmentMap(tempMap);
          setDepartments(toSet);
        }
      }

      const subjectsResponse = await getSubjectList();
      if (subjectsResponse.state === 'success') {
        const { data } = subjectsResponse;
        if (data) {
          const tempMap: Record<string, number> = {};
          const toSet = data.map((opt) => {
            tempMap[`${opt.name}`] = opt.id;
            return opt.name;
          });
          setSubjectMap(tempMap);
          setSubjects(toSet);
        }
      }
    })();
  }, []);

  const onSubmit = async (data: Fields) => {
    const subIds = subjectAutoComplete.map((key: string) => subjectMap[key]);
    const depIds = departmentAutoComplete.map((key: string) => departmentMap[key]);
    const params = {
      slug: props.mode === 'edit' && props.slug,
      // eslint-disable-next-line
        fullname: `${data.lastName} ${data.firstName}` + (data.patronymic ? ` ${data.patronymic}` : ''),
      birthday: data.birthday || null,
      departments: depIds,
      subjects: subIds,
      image: imageId,
    };

    if (props.mode === 'edit') {
      const response = await editProfessor(props.slug, params);
      if (response.state === 'success') {
        const detailes = response.data;
        history.replace(`${detailes.slug}`);
        props.onEditHandle(response.data);
        props.handlePopoverClose();
      } else {
        // todo popover
        if (response.data) {
          // чо тут делать?
          setErrorString('какая то ошибка');
        }
        setAnchorErrorEl(window.document.body);
        setErrorString('Что-то пошло не так');
      }
    } else {
      const response = await createProfessor(params);
      if (response.state === 'success') {
        history.replace(from);
      } else {
        // todo popover
        if (response.data) {
          // чо тут делать?
          setErrorString('какая то ошибка');
        }
        setAnchorErrorEl(window.document.body);
        setErrorString('Что-то пошло не так');
      }
    }
  };

  const onDepartmentClick = () => {
    setAnchorEl(window.document.body);
  };

  const onSubjectClick = () => {
    setAnchorSubjectEl(window.document.body);
  };

  const departmentNameChange = (
    evt: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    setDepartmentName(evt.target.value);
  };

  const departmentNumberChange = (
    evt: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    setDepartmentNumber(evt.target.value);
  };

  const departmentAutoCompleteChange = (evt: React.ChangeEvent<{}>, value: string[]) => {
    setDepartmentAutoComplete(value);
  };

  const subjectNameChange = (
    evt: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    setSubjectName(evt.target.value);
  };

  const subjectDescriptionChange = (
    evt: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => {
    setSubjectDescription(evt.target.value);
  };

  const subjectAutoCompleteChange = (evt: React.ChangeEvent<{}>, value: string[]) => {
    setSubjectAutoComplete(value);
  };

  const onPopoverSubmit = async (evt: React.FormEvent<HTMLFormElement>) => {
    evt.preventDefault();
    const response = await addDepartment({
      number: departmentNumber,
      name: departmentName,
      is_active: true,
    });
    if (response.state === 'success') {
      const { data } = response;
      setDepartmentMap((prev) => ({
        ...prev,
        [`${data.number}. ${data.name}`]: data.id,
      }));
      setDepartments((prev) => ([
        ...prev,
        `${data.number}. ${data.name}`,
      ]));
    } else {
      // todo popover
      if (response.data) {
        // чо тут делать?
        setErrorString('какая то ошибка');
      }
      setAnchorErrorEl(window.document.body);
      setErrorString('Что-то пошло не так');
    }

    setAnchorEl(null);
  };

  const onPopoverSubjectSubmit = async (evt: React.FormEvent<HTMLFormElement>) => {
    evt.preventDefault();
    const response = await addSubject({
      name: subjectName,
      description: subjectDescription,
    });
    if (response.state === 'success') {
      const { data } = response;
      setSubjectMap((prev) => ({
        ...prev,
        [`${data.name}`]: Number(data.id),
      }));
      setSubjects((prev) => ([
        ...prev,
        `${data.name}`,
      ]));
    } else {
      // todo popover
      if (response.data) {
        // чо тут делать?
        setErrorString('какая то ошибка');
      }
      setAnchorErrorEl(window.document.body);
      setErrorString('Что-то пошло не так');
    }

    setAnchorSubjectEl(null);
  };

  const handleError = (error: Error): Messages => {
    const result: Messages = {};
    Object.keys(error).forEach((val) => {
      const key = val as keyof Error;
      if (error[key]?.type === 'required') {
        result[key] = t(MESSAGES.REQUIRED);
      } else if (error[key]?.type === 'pattern') {
        result[key] = t(MESSAGES.WRONG_NAME_PATTERN);
      }
    });
    return result;
  };

  const messages = handleError(errors);

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handleErrorPopoverClose = () => {
    setAnchorErrorEl(null);
  };

  const handleSubjectPopoverClose = () => {
    setAnchorSubjectEl(null);
  };

  const popoverDepartment = (
    <form onSubmit={onPopoverSubmit} className={classes.popover} noValidate>
      <TextField
        fullWidth
        id="departmentNumber"
        label="Номер кафедры"
        name="departmentNumber"
        variant="outlined"
        margin="normal"
        value={departmentNumber}
        onChange={departmentNumberChange}
        inputRef={register({
          required: true,
        })}
        InputLabelProps={{
          shrink: true,
        }}
      />
      <TextField
        fullWidth
        id="departmentName"
        label="Название кафедры"
        name="departmentName"
        variant="outlined"
        margin="normal"
        inputRef={register({
          required: true,
        })}
        InputLabelProps={{
          shrink: true,
        }}
        value={departmentName}
        onChange={departmentNameChange}
      />
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
      >
        Отправить
      </Button>
    </form>
  );

  const popoverSubject = (
    <form onSubmit={onPopoverSubjectSubmit} className={classes.popover} noValidate>
      <TextField
        fullWidth
        id="subjectName"
        label="Название предмета"
        name="subjectName"
        variant="outlined"
        margin="normal"
        value={subjectName}
        onChange={subjectNameChange}
        inputRef={register({
          required: true,
        })}
        InputLabelProps={{
          shrink: true,
        }}
      />
      <TextField
        fullWidth
        id="subjectDiscription"
        label="Описание"
        name="subjectDiscription"
        variant="outlined"
        margin="normal"
        inputRef={register({
          required: true,
        })}
        InputLabelProps={{
          shrink: true,
        }}
        value={subjectDescription}
        onChange={subjectDescriptionChange}
      />
      <Button
        type="submit"
        fullWidth
        variant="contained"
        color="primary"
      >
        Отправить
      </Button>
    </form>
  );

  const errorPopover = (
    <div className={classes.popover}>
      {errorString}
    </div>
  );

  const onAddImageClick = () => {
    setAnchorUploadEl(window.document.body);
  };

  const handleUploadPopoverClose = () => {
    setAnchorUploadEl(null);
  };

  const uploadHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPicked(true);
    const { target } = event;
    if (target && target.files) {
      setImage(target.files[0]);
    }
  };

  const handleUploadClick = async () => {
    if (image) {
      if (image.size < 100 * 1024 * 1024) {
        const formData = new FormData();
        formData.append('image', image);
        const response = await uploadImage(formData);
        if (response.state === 'success') {
          setAnchorUploadEl(null);
          setImageId(response.data.id);
        } else {
          setAnchorUploadEl(null);
          setErrorString('Не удалось загрузить изображение');
          setAnchorErrorEl(window.document.body);
        }
      } else {
        setErrorString('Файл должен быть меньше 100мб');
        setAnchorErrorEl(window.document.body);
      }
    }
  };

  const imageUploadPopover = (
    <div className={classes.popover}>
      <Typography>
        Загрузите файл
      </Typography>
      <input type="file" name="file" onChange={uploadHandler} />
      <div className={classes.buttonContainer}>
        {isPicked && (
          <Button
            variant="contained"
            color="primary"
            onClick={handleUploadClick}
          >
            Отправить
          </Button>
        )}
        <Button
          variant="contained"
          color="primary"
          onClick={handleUploadPopoverClose}
        >
          Отмена
        </Button>
      </div>
    </div>
  );

  return (
    <div className={classes.popover}>
      <form onSubmit={handleSubmit(onSubmit)} noValidate>
        <TextField
          required
          fullWidth
          defaultValue={props.mode === 'edit' ? props.lastName : undefined}
          id="lastName"
          label={t(MESSAGES.LAST_NAME)}
          name="lastName"
          variant="outlined"
          margin="normal"
          inputRef={
            register({
              required: true,
              pattern: /^[А-Я]{1}[а-я]+$/,
            })
          }
          InputLabelProps={{
            shrink: true,
          }}
          error={!!messages.lastName}
          helperText={messages.lastName}
        />
        <TextField
          required
          fullWidth
          defaultValue={props.mode === 'edit' ? props.firstName : undefined}
          id="first_name"
          label={t(MESSAGES.FIRST_NAME)}
          name="firstName"
          variant="outlined"
          margin="normal"
          inputRef={
            register({
              required: true,
              pattern: /^[А-Я]{1}[а-я]+$/,
            })
          }
          InputLabelProps={{
            shrink: true,
          }}
          error={!!messages.firstName}
          helperText={messages.firstName}
        />
        <TextField
          fullWidth
          defaultValue={props.mode === 'edit' ? props.patronimic : undefined}
          id="patronymic"
          label={t(MESSAGES.PATRONYMIC)}
          name="patronymic"
          variant="outlined"
          margin="normal"
          inputRef={
            register({
              pattern: /^[А-Я]{1}[а-я]+$/,
            })
          }
          InputLabelProps={{
            shrink: true,
          }}
          error={!!messages.patronymic}
          helperText={messages.patronymic}
        />
        <TextField
          fullWidth
          defaultValue={props.mode === 'edit' ? props.birthday : undefined}
          type="date"
          id="birthday"
          label={t(MESSAGES.BIRTHDAY)}
          name="birthday"
          variant="outlined"
          margin="normal"
          inputRef={register}
          InputLabelProps={{
            shrink: true,
          }}
        />
        <Autocomplete
          id="departments"
          open={openDepartment}
          onOpen={() => {
            setOpenDepartment(true);
          }}
          onClose={() => {
            setOpenDepartment(false);
          }}
          onChange={departmentAutoCompleteChange}
          multiple
          options={departments}
          className="normal"
          value={departmentAutoComplete}
          renderInput={(params: AutocompleteRenderInputParams) => (
            <TextField
                // eslint-disable-next-line
                {...params}
              required
              id="departmentstext"
              label="Кафедры"
              variant="outlined"
              margin="normal"
              type="textArea"
            />
          )}
        />
        <Button
          type="button"
          onClick={onDepartmentClick}
          fullWidth
        >
          Если вы не нашли свою кафедру, введите ее здесь
        </Button>

        <Autocomplete
          id="subject"
          open={openSubject}
          onOpen={() => {
            setOpenSubject(true);
          }}
          onClose={() => {
            setOpenSubject(false);
          }}
          onChange={subjectAutoCompleteChange}
          multiple
          options={subjects}
          className="normal"
          value={subjectAutoComplete}
          renderInput={(params: AutocompleteRenderInputParams) => (
            <TextField
                // eslint-disable-next-line
                {...params}
              required
              id="departmentstext"
              label="Предметы"
              variant="outlined"
              margin="normal"
              type="textArea"
            />
          )}
        />
        <Button
          type="button"
          onClick={onSubjectClick}
          fullWidth
        >
          Если вы не нашли предмет, введите его здесь
        </Button>
        <Button
          type="button"
          onClick={onAddImageClick}
          fullWidth
        >
          Добавьте изображение
        </Button>
        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="primary"
        >
          {t(MESSAGES.SUBMIT)}
        </Button>

      </form>
      <Popover
        id="editProfDepartment"
        open={openPopover}
        anchorEl={anchorEl}
        onClose={handlePopoverClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        {popoverDepartment}
      </Popover>
      <Popover
        id="editProfSubject"
        open={openSubjectPopover}
        anchorEl={anchorSubjectEl}
        onClose={handleSubjectPopoverClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        {popoverSubject}
      </Popover>
      <Popover
        id="errorPopover"
        open={openErrorPopover}
        anchorEl={anchorErrorEl}
        onClose={handleErrorPopoverClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        {errorPopover}
      </Popover>
      <Popover
        id="deleteSubjectRelation"
        open={popoverUploadOpen}
        onClose={handleUploadPopoverClose}
        anchorEl={anchorUploadEl}
        anchorOrigin={{
          vertical: 'center',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'center',
          horizontal: 'center',
        }}
      >
        {imageUploadPopover}
      </Popover>
    </div>
  );
};

ProfessorForm.defaultProps = {
  birthday: null,
};

export default ProfessorForm;
