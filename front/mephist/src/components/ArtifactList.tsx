import React, { useState } from 'react';

// import fileDownload from 'js-file-download';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import DeleteIcon from '@material-ui/icons/Delete';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import Button from '@material-ui/core/Button';
import Popover from '@material-ui/core/Popover';
import IconButton from '@material-ui/core/IconButton';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import ArchiveIcon from '@material-ui/icons/Archive';

import { ArtifactParams, downloadArtifact } from '../api/artifact';

interface Props {
  professor: string;
  subjectId: number;
  list: ArtifactParams[];
  // eslint-disable-next-line
  handleDelete: (id: number) => Promise<void>;
}

interface ElementProps extends ArtifactParams {
   // eslint-disable-next-line
  handleDelete: (id: number) => Promise<void>;
}

const useStyles = makeStyles(() => ({
  element: {
    display: 'flex',
    justifyContent: 'flex-start',
  },
  container: {
    width: '100%',
    marginTop: '20px',
  },
  elementContainer: {
    width: '100%',
    padding: '10px',
    display: 'flex',
    justifyContent: 'space-between',
    marginLeft: '20px',
  },
  textContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '60%',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: '20px',
  },
  popover: {
    padding: '15px',
  },
}));

const ArtifactElement: React.FC<ElementProps> = (props: ElementProps) => {
  const {
    id,
    name,
    date_created,
    handleDelete,
  } = props;
  const classes = useStyles();

  const [anchorDeleteEl, setAnchorDeleteEl] = useState<HTMLElement | null>(null);
  const popoverDeleteOpen = Boolean(anchorDeleteEl);

  const onDeleteClick = () => {
    setAnchorDeleteEl(window.document.body);
  };

  const handleDeletePopoverClick = () => {
    handleDelete(id);
    setAnchorDeleteEl(null);
  };

  const handleDeletePopoverClose = () => {
    setAnchorDeleteEl(null);
  };

  // todo херня нужен тип файла
  const parseIcon = () => {
    if (name.includes('.pdf')) {
      return (
        <PictureAsPdfIcon />
      );
    }
    return (
      <ArchiveIcon />
    );
  };

  const prettifyDate = () => {
    const date = new Date(date_created);
    return date.toLocaleString('ru-RU', { year: 'numeric', month: 'numeric', day: 'numeric' });
  };

  const onDownLoadClick = async () => {
    const response = await downloadArtifact(id);
    if (response.state === 'success') {
      const { data } = response;
      const fileURL = URL.createObjectURL(data);
      window.open(fileURL);
    }
  };

  const deletePopover = (
    <Popover
      open={popoverDeleteOpen}
      onClose={handleDeletePopoverClose}
      anchorEl={anchorDeleteEl}
      anchorOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
    >
      <div className={classes.popover}>
        <Typography>
          Вы уверены, что хотите удалить артефакт?
        </Typography>
        <div className={classes.buttonContainer}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleDeletePopoverClick}
          >
            Удалить
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={handleDeletePopoverClose}
          >
            Отмена
          </Button>
        </div>
      </div>
    </Popover>
  );

  return (
    <div key={id}>
      <ListItem alignItems="flex-start">
        <div className={classes.elementContainer}>
          <div className={classes.textContainer}>
            {parseIcon()}
            <div>
              <Typography variant="subtitle1" className={classes.element} noWrap>{name}</Typography>
              <Typography variant="subtitle2" className={classes.element} noWrap>{prettifyDate()}</Typography>
            </div>
          </div>
          <div className={classes.buttonContainer}>
            <IconButton onClick={onDownLoadClick}>
              <CloudDownloadIcon />
            </IconButton>
            <IconButton onClick={onDeleteClick}>
              <DeleteIcon />
            </IconButton>
          </div>
        </div>
      </ListItem>
      <Divider />
      {deletePopover}
    </div>
  );
};

const ArtefactsList: React.FC<Props> = (props: any) => {
  const classes = useStyles();
  const {
    subjectId,
    professor,
    list,
    handleDelete,
  } = props;

  return (
    <div className={classes.container}>
      {list.map((data: ArtifactParams) => {
        const {
          id, name, date_created, file_hash, artifact,
        } = data;
        return (
          <ArtifactElement
            key={id}
            id={id}
            name={name}
            date_created={date_created}
            handleDelete={handleDelete}
            file_hash={file_hash}
            professor={professor}
            subject={subjectId}
            artifact={artifact}
          />
        );
      })}
    </div>
  );
};

export default ArtefactsList;
