import React, { FC, useState } from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import ThumbDownIcon from '@material-ui/icons/ThumbDown';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';
import ThumbDownAltOutlinedIcon from '@material-ui/icons/ThumbDownAltOutlined';
import Checkbox, { CheckboxProps } from '@material-ui/core/Checkbox';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreIcon from '@material-ui/icons/MoreVert';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import { Feedback, UpdateFeedbackRequest } from '../../api/professor';
import CommentForm from './CommentForm';
import { AxiosReply } from '../../utils/rest';

const RedCheckbox = withStyles({
  root: {
    color: 'red',
  },
  checked: {},
// eslint-disable-next-line react/jsx-props-no-spreading
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

const GreenCheckbox = withStyles({
  root: {
    color: 'green',
  },
  checked: {},
// eslint-disable-next-line react/jsx-props-no-spreading
})((props: CheckboxProps) => <Checkbox color="default" {...props} />);

const useStyles = makeStyles({
  root: {
    padding: 5,
    margin: '10px 0',
  },
  cardContent: {
    display: 'flex',

    '& .comment': {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
      padding: '0 15px',

      '& .comment-header': {
        display: 'flex',
        justifyContent: 'space-between',
      },

      '& .comment__inner': {
        whiteSpace: 'pre-line',
        wordBreak: 'break-word',
        paddingTop: 5,
        paddingBottom: 15,
      },

      '& .comment-header__counter': {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        fontSize: '1.1em',
        fontWeight: 700,
        padding: '0 5px',
      },
    },
  },
  commentHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottom: '1px solid rgba(0, 0, 0, 0.12)',
    padding: '10px 0px 10px 31px',
  },
  commentDate: {
    color: 'rgba(31,31,31,.6)',
  },
  commentUsername: {
    fontSize: '1.25em',
    paddingBottom: '5px',
    fontWeight: 700,
  },
  commentTitle: {
    fontSize: '1.25em',
    fontWeight: 700,
  },
  commentFooter: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  checkboxItem: {
    marginRight: 0,
    marginLeft: 0,
  },
});

type Props = {
  comment: Feedback;
   // eslint-disable-next-line
  onDelete: (id: number) => Promise<void>;
   // eslint-disable-next-line
  onUpdate: (id: number, updateFields: UpdateFeedbackRequest) => AxiosReply<Feedback>;
}

const Comments: FC<Props> = ({ comment, onDelete, onUpdate }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [isEditMode, setEditMode] = useState(false);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    const element = event.currentTarget;
    setAnchorEl(element);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const isMenuOpen = Boolean(anchorEl);

  // const onClick = (newRate: Rate) => async () => {
  //   const rate = newRate === comment.rate ? 0 : newRate;

  //   const res = await postCommentRate(comment.id, { rate });
  //   if (res.state === 'success') {
  //     updateCommentRate(comment.id, rate);
  //   }
  // };

  const openEditForm = () => {
    setEditMode(true);
    setAnchorEl(null);
  };

  const closeEditForm = () => {
    setEditMode(false);
  };

  const deleteComment = () => {
    setAnchorEl(null);
    onDelete(comment.id);
  };

  const updateComment = async (title: string, content: string) => {
    const res = await onUpdate(comment.id, { title, content });
    if (res.state === 'success') {
      setEditMode(false);
    }
  };

  const prettifyDate = () => {
    const date = new Date(comment.datetime);

    return date.toLocaleString('ru-RU', { year: 'numeric', month: 'numeric', day: 'numeric' });
  };

  const popover = (
    <Menu
      getContentAnchorEl={null}
      open={isMenuOpen}
      onClose={handleClose}
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      disablePortal
    >
      <MenuItem onClick={openEditForm}>
        Редактировать
      </MenuItem>
      <MenuItem onClick={deleteComment}>
        Удалить
      </MenuItem>
    </Menu>
  );

  if (isEditMode) {
    return (
      <CommentForm feedback={comment} onSubmit={updateComment} onClose={closeEditForm} />
    );
  }

  return (
    <Card className={classes.root} variant="outlined">
      <div className={classes.commentHeader}>
        <div>
          <div className={classes.commentUsername}>
            {comment.username}
          </div>
          <div className={classes.commentDate}>
            { prettifyDate() }
          </div>
        </div>

        <IconButton
          onClick={handleClick}
          aria-controls="menu-appbar"
          aria-haspopup
        >
          <MoreIcon />
        </IconButton>
        {popover}
      </div>
      <CardContent className={classes.cardContent}>
        <div className="comment">
          <div className={classes.commentTitle}>
            {comment.title}
          </div>
          <div className="comment__inner">
            {comment.content}
          </div>
          <div className={classes.commentFooter}>

            <Button size="small" variant="outlined">
              Ответить
            </Button>
            <FormGroup
              row
              onClick={(event) => {
                event.stopPropagation();
              }}
            >

              <FormControlLabel
                label=""
                className={classes.checkboxItem}
                control={(
                  <GreenCheckbox
                    icon={<ThumbUpAltOutlinedIcon />}
                    checkedIcon={<ThumbUpIcon />}
                  />
                )}
              />
              <span className="comment-header__counter">
                0
              </span>
              <FormControlLabel
                label=""
                className={classes.checkboxItem}
                control={(
                  <RedCheckbox
                    icon={<ThumbDownAltOutlinedIcon />}
                    checkedIcon={<ThumbDownIcon />}
                  />
                )}
              />
            </FormGroup>
          </div>
        </div>
      </CardContent>
    </Card>
  );
};

export default Comments;
