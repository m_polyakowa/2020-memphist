import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import CommentItem from './CommentItem';
import {
  addProfessorFeedback,
  Feedback,
  getProfessorFeedback,
  deleteProfessorFeedback,
  updateProfessorFeedback,
  UpdateFeedbackRequest,
} from '../../api/professor';
import CommentForm from './CommentForm';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    maxWidth: 900,
    margin: 'auto',
    padding: 5,
    marginTop: 15,
  },
  textField: {
    color: '#efeff3',
  },
  buttonWrapper: {
    display: 'flex',
    justifyContent: 'flex-end',
    margin: '10px 0',
  },
});

type Props = {
  slug: string;
  subtitle?: string;
}

const Comments: React.FC<Props> = ({ slug, subtitle }) => {
  const classes = useStyles();
  const [comments, setComments] = useState<Feedback[]>([]);

  useEffect(() => {
    const fetchComments = async () => {
      const res = await getProfessorFeedback(slug);

      if (res.state === 'success') {
        setComments(res.data);
      }
    };
    fetchComments();
  }, [slug]);

  const addFeedback = async (title: string, content: string) => {
    if (title === '') {
      return;
    }

    if (content === '') {
      return;
    }

    const normalizedContent = content.replace(/(^[ \t]*\n+)/gm, '\n').trim();

    const res = await addProfessorFeedback({ slug, title, content: normalizedContent });

    if (res.state === 'success') {
      setComments([
        res.data,
        ...comments,
      ]);
    }
  };

  const deleteFeedback = async (id: number) => {
    const res = await deleteProfessorFeedback(id);
    if (res.state === 'success') {
      setComments(comments.filter((comment) => comment.id !== id));
    }
  };

  const updateFeedback = async (id: number, params: UpdateFeedbackRequest) => {
    const res = await updateProfessorFeedback(id, params);
    if (res.state === 'success') {
      const updatedFeedback = res.data;
      setComments(comments.map((comment) => {
        if (comment.id === id) {
          return { ...updatedFeedback };
        }
        return comment;
      }));
    }

    return res;
  };

  // const updateCommentRate = (commentID: number, rate: Rate) => {
  //   setComments(
  //     comments.map((comment) => (comment.id
  //       === commentID
  //       ? { ...comment, sumRate: comment.sumRate - comment.rate + rate, rate } : comment)),
  //   );
  // };

  return (
    <div className={classes.root}>
      {subtitle && (
        <Typography variant="h6">
          {subtitle}
        </Typography>
      )}
      <CommentForm onSubmit={addFeedback} />
      <div>
        {comments.map((comment) => (
          <CommentItem
            comment={comment}
            onDelete={deleteFeedback}
            // @ts-ignore
            onUpdate={updateFeedback}
            key={comment.id}
          />
        ))}
      </div>
    </div>
  );
};

export default Comments;
