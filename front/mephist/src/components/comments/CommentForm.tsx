import React, { FC, useEffect, useState } from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/icons/Send';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import { Feedback } from '../../api/professor';

interface Props {
   // eslint-disable-next-line
  onSubmit: (title: string, content: string) => Promise<void>;
  onClose?: () => void;
  feedback?: Feedback;
}

const FeedbackForm: FC<Props> = ({ feedback, onSubmit, onClose = () => {} }) => {
  const [isEditMode, setEditMode] = useState(false);
  const [content, setContent] = useState('');
  const [title, setTitle] = useState('');

  useEffect(() => {
    if (feedback !== undefined) {
      setEditMode(true);
      setContent(feedback.content);
      setTitle(feedback.title);
    }
  }, []);

  const handleTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };

  const handleContent = (event: React.ChangeEvent<HTMLInputElement>) => {
    setContent(event.target.value);
  };

  const submit = async () => {
    await onSubmit(title, content);
    setTitle('');
    setContent('');
  };

  return (
    <Card variant="outlined">
      <CardContent>
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          label="Заголовок"
          value={title}
          onInput={handleTitle}
        />
        <TextField
          label="Отзыв"
          multiline
          variant="outlined"
          fullWidth
          margin="normal"
          rows={3}
          rowsMax={6}
          value={content}
          onInput={handleContent}
        />
        <div>
          <Button
            variant="contained"
            color="primary"
            endIcon={<Icon>send</Icon>}
            onClick={submit}
          >
            {isEditMode ? 'Сохранить' : 'Отправить'}
          </Button>
          {
            isEditMode && (
              <Button
                variant="contained"
                onClick={onClose}
              >
                Отменить
              </Button>
            )
          }
        </div>
      </CardContent>
    </Card>
  );
};

export default FeedbackForm;
