import React from 'react';
import { useHistory } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import {
  getProfessorsList, getProfessorDetails, ProfessorListElementParams,
} from '../api/professor';

interface Props {
  list: ProfessorListElementParams[];
}

const useStyles = makeStyles(() => ({
  professorElement: {
    display: 'flex',
    justifyContent: 'center',
  },
  popover: {
    height: '80vh',
    padding: '30px',
  },
}));

const RenderProfessorsList: React.FC<Props> = (props: Props) => {
  const classes = useStyles();
  const {
    list = [],
  } = props;

  const history = useHistory();

  const onClick = (slug: string) => {
    getProfessorDetails(slug)
      .then(() => {
        history.push({ pathname: `/professors/${slug}`, state: { slug } });
      })
      .catch((e) => {
        console.error(e);
      });
  };

  return (
    <div className={classes.popover}>
      {list.map((data: ProfessorListElementParams) => {
        const { fullname, slug } = data;
        return (
          <React.Fragment key={slug}>
            <ListItem button alignItems="flex-start" onClick={() => onClick(slug)}>
              <ListItemText>
                <Typography className={classes.professorElement}>{fullname}</Typography>
              </ListItemText>
            </ListItem>
            <Divider />
          </React.Fragment>
        );
      })}
    </div>
  );
};

const ProfessorsList = async () => {
  const response = await getProfessorsList();

  if (response.state === 'success') {
    if (!response.data) {
      return (
        <div style={{ width: '80vw', height: '80vh', textAlign: 'center' }}>Здесь пока ничего нет</div>
      );
    }
    return <RenderProfessorsList list={response.data} />;
  }
  return (
    <div style={{ width: '80vw', height: '80vh', textAlign: 'center' }}>Произошла ошибка</div>
  );
};

export default ProfessorsList;
