import React from 'react';
import { useHistory } from 'react-router-dom';

import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import {
  getTopBestProfessors,
  getTopWorstProfessors,
  TopParams,
  getProfessorDetails,
} from '../api/professor';

interface Props {
  best: TopParams[];
  worst: TopParams[];
}

const useStyles = makeStyles(() => ({
  professorElement: {
    display: 'flex',
    justifyContent: 'center',
  },
  container: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '80vw',
  },
  list: {
    display: 'flex',
    flexDirection: 'column',
    padding: '30px',
    border: '1px solid rgba(0,0,0,0.5)',
    borderRadius: '3px',
    backgroundColor: '#fff',
  },
}));

const RenderProfessorsTop: React.FC<Props> = (props: Props) => {
  const classes = useStyles();
  const {
    best,
    worst,
  } = props;

  const history = useHistory();

  const onClick = (slug: string) => {
    getProfessorDetails(slug)
      .then(() => {
        history.push({ pathname: `/professors/${slug}`, state: { slug } });
      })
      .catch((e) => {
        console.error(e);
      });
  };

  return (
    <div className={classes.container}>
      <div className={classes.list}>
        <Typography variant="h6" align="center">Топ лучших преподавателей</Typography>
        {best.map((data: TopParams, idx: number) => {
          const { fullname, slug } = data;
          return (
            <React.Fragment key={slug}>
              <ListItem button alignItems="flex-start" onClick={() => onClick(slug)}>
                <ListItemText>
                  <Typography className={classes.professorElement}>
                    {`${idx + 1}. ${fullname}`}
                  </Typography>
                </ListItemText>
              </ListItem>
              <Divider />
            </React.Fragment>
          );
        })}
      </div>
      <div className={classes.list}>
        <Typography variant="h6" align="center">Топ худших преподавателей</Typography>
        {worst.map((data: TopParams, idx: number) => {
          const { fullname, slug } = data;
          return (
            <React.Fragment key={slug}>
              <ListItem button alignItems="flex-start" onClick={() => onClick(slug)}>
                <ListItemText>
                  <Typography className={classes.professorElement}>
                    {`${idx + 1}. ${fullname}`}
                  </Typography>
                </ListItemText>
              </ListItem>
              <Divider />
            </React.Fragment>
          );
        })}
      </div>
    </div>
  );
};

const ProfessorsTop = async () => {
  const best = await getTopBestProfessors();
  const worst = await getTopWorstProfessors();

  if (best.state === 'success' && worst.state === 'success') {
    if (!best.data || !worst.data) {
      return (
        <Typography>Здесь пока ничего нет</Typography>
      );
    }
    return <RenderProfessorsTop best={best.data} worst={worst.data} />;
  }
  return (
    <Typography>Произошла ошибка! Невозможно отобразить топ преподавателей</Typography>
  );
};

export default ProfessorsTop;
