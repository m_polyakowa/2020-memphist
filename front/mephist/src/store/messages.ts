export default {
  FIRST_NAME: 'Имя',
  LAST_NAME: 'Фамилия',
  PATRONYMIC: 'Отчество',
  BIRTHDAY: 'Дата рождения',
  DEPARTMENT: 'Кафедра',
  DESCRIPTION: 'Описание',
  REQUIRED: 'Это поле обязательно заполнения',
  WRONG_NAME_PATTERN: 'Введите текст на кириллице с заглавной буквы',
  INVALID_EMAIL: 'Невалидный email',
  SUBMIT: 'Отправить',
  PROFESSORS: 'Преподаватели',
  CREATE_PROFESSOR: 'Добавить преподавателя',
};
