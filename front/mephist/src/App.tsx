import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import AuthProvider from './components/auth/AuthProvider';
import Login from './routes/Login';
import SignUp from './routes/SignUp';
import Activate from './routes/Activate';
import Main from './routes/Main';
import TopMenu from './layouts/TopMenu';
import AppMiddleware from './AppMiddleware';
import CreateProfessorForm from './routes/CreateProfessor';
import ProfessorProfile from './routes/ProfessorProfile';
import './utils/i18n';

function App() {
  return (
    <AuthProvider>
      <AppMiddleware>
        <Router>
          <Switch>
            <Route path="/login">
              <Login />
            </Route>
            <Route path="/sign-up">
              <SignUp />
            </Route>
            <Route path="/account/activate/:uidb64/:token">
              <Activate />
            </Route>
            <Route path="/create-professor">
              <CreateProfessorForm />
            </Route>
            <Route path="/professors/:slug">
              <ProfessorProfile />
            </Route>
            <Route path="/">
              <TopMenu>
                <Main />
              </TopMenu>
            </Route>
          </Switch>
        </Router>
      </AppMiddleware>
    </AuthProvider>
  );
}

export default App;
