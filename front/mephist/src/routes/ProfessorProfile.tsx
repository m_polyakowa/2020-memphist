import React, { useState, useEffect } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import StarRatingComponent from 'react-star-rating-component';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import BackIcon from '@material-ui/icons/ArrowBack';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Popover from '@material-ui/core/Popover';

import { makeStyles } from '@material-ui/core/styles';

import {
  ExtendedProfessorParams,
  getProfessorDetails,
  deleteProfessor,
  editProfessor,
} from '../api/professor';

import {
  updateProfessorRating,
  getUserProfessorRating,
  RatingParams,
} from '../api/rating';

import ProfessorForm from '../components/ProfessorForm';
import SubjectList from '../components/SubjectList';
import Comments from '../components/comments/Comments';
import { useAuth } from '../components/auth/AuthProvider';

interface LocationType {
  slug: string;
  from: {
    pathname: string;
  };
}

const useStyles = makeStyles((theme) => ({
  // necessary for content to be below app bar
  base: theme.mixins.toolbar,
  container: {
    padding: '15px',
  },
  toolbar: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  assessment: {
    display: 'flex',
    flexDirection: 'column',
    fontSize: '20px',
    marginBottom: '20px',
  },
  button: {
    textOverflow: 'elipsis',
    alignSelf: 'flex-start',
  },
  starsContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '12px',
    alignItems: 'flex-start',
  },
  deleteContainer: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    padding: '30px',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'space-evenly',
    marginTop: '20px',
  },
  image: {
    width: '360px',
    height: 'auto',
  },
}));

export default function ProfessorProfile() {
  const location = useLocation<LocationType>();
  const slug = location.pathname.split('/')[2];
  const initialParams: ExtendedProfessorParams = {
    fullname: '',
    departments: [],
    subjects: [],
    department_details: [],
    subject_details: [],
    slug,
    id: -1,
  };
  const [params, setParams] = useState<ExtendedProfessorParams>(initialParams);
  const [characterRating, setCharacterRating] = useState(5);
  const [qualityRating, setQualityRating] = useState(5);
  const [examRating, setExamRating] = useState(5);
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [anchorDeleteEl, setAnchorDeleteEl] = useState<HTMLElement | null>(null);
  const [anchorErrorEl, setAnchorErrorEl] = useState<HTMLElement | null>(null);
  const [error, setError] = useState('Что-то пошло не так');
  const [chDisabled, setChDisabled] = useState(false);
  const [quDisabled, setQuDisabled] = useState(false);
  const [exDisabled, setExDisabled] = useState(false);

  const open = Boolean(anchorEl);
  const idPopover = open ? 'simple-popover' : undefined;
  const classes = useStyles();
  const history = useHistory();
  const { from } = { from: { pathname: '/' } };
  const openDelete = Boolean(anchorDeleteEl);
  const openError = Boolean(anchorErrorEl);
  const { user } = useAuth();
  const isStaff = user?.is_staff;

  useEffect(() => {
    (async () => {
      const response = await getProfessorDetails(params.slug);
      if (response.state === 'success') {
        setParams(response.data);

        const { id } = response.data;

        const chRatingResponse = await getUserProfessorRating('ch', id);
        if (chRatingResponse.state === 'success' && chRatingResponse.data) {
          const { value } = chRatingResponse.data;
          setChDisabled(true);
          setCharacterRating(value);
        }

        const quRatingResponse = await getUserProfessorRating('qu', id);
        if (quRatingResponse.state === 'success' && quRatingResponse.data) {
          const { value } = quRatingResponse.data;
          setQuDisabled(true);
          setQualityRating(value);
        }

        const exRatingResponse = await getUserProfessorRating('ex', id);
        if (exRatingResponse.state === 'success' && exRatingResponse.data) {
          const { value } = exRatingResponse.data;
          setExDisabled(true);
          setExamRating(value);
        }
      }
    })();
  }, []);

  const onCharacterStarClick = (nextValue: number) => {
    setCharacterRating(nextValue);
  };

  const onQualityStarClick = (nextValue: number) => {
    setQualityRating(nextValue);
  };

  const onExamStarClick = (nextValue: number) => {
    setExamRating(nextValue);
  };

  const handleBackClick = () => {
    history.replace(from);
  };

  const handleDeleteClick = () => {
    setAnchorDeleteEl(window.document.body);
  };

  const handleDeletePopoverClick = async () => {
    const response = await deleteProfessor(slug);
    if (response.state === 'success') {
      history.replace(from);
    } else {
      setError('Не удалось удалить профиль преподавателя');
      setAnchorErrorEl(window.document.body);
    }
  };

  const handleEditClick = () => {
    setAnchorEl(window.document.body);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const handleDeletePopoverClose = () => {
    setAnchorDeleteEl(null);
  };

  const handleErrorPopoverClose = () => {
    setAnchorErrorEl(null);
  };

  const onCharacterStarButtonClick = async () => {
    const rating: RatingParams = {
      user_type: 'ch',
      value: characterRating,
      content_id: params.id,
    };
    const response = await updateProfessorRating(rating);
    if (response.state === 'failed') {
      setError('Не удалось проставить оценку');
      setAnchorErrorEl(window.document.body);
    } else {
      setChDisabled(true);
    }
  };

  const onQualityStarButtonClick = async () => {
    const rating: RatingParams = {
      user_type: 'qu',
      value: qualityRating,
      content_id: params.id,
    };
    const response = await updateProfessorRating(rating);
    if (response.state === 'failed') {
      setError('Не удалось проставить оценку');
      setAnchorErrorEl(window.document.body);
    } else {
      setQuDisabled(true);
    }
  };

  const onExamStarButtonClick = async () => {
    const rating: RatingParams = {
      user_type: 'ex',
      value: examRating,
      content_id: params.id,
    };
    const response = await updateProfessorRating(rating);
    if (response.state === 'failed') {
      setError('Не удалось проставить оценку');
      setAnchorErrorEl(window.document.body);
    } else {
      setExDisabled(true);
    }
  };

  const EditSubjectList = async (subjectId: number) => {
    const tmp = { ...params };
    tmp.subjects = tmp.subjects.filter((v) => v !== subjectId);
    tmp.subject_details = tmp.subject_details.filter((v) => v.id !== subjectId);
    const response = await editProfessor(tmp.slug, tmp);
    if (response.state === 'success') {
      setParams(tmp);
    } else {
      setError('Не удалось удалить дисциплину');
      setAnchorErrorEl(window.document.body);
    }
  };

  // todo???
  const names = params.fullname.split(' ');
  const lastName = names[0];
  const firstName = names[1];
  const patronymic = names[2];

  const deletePopover = (
    <Popover
      id="deletePopover"
      open={openDelete}
      anchorEl={anchorDeleteEl}
      onClose={handleDeletePopoverClose}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'center',
      }}
    >
      <div className={classes.deleteContainer}>
        <Typography>
          Вы уверены, что хотите удалить профиль преподавателя?
        </Typography>
        <div className={classes.buttonContainer}>
          <Button
            variant="contained"
            color="primary"
            onClick={handleDeletePopoverClick}
          >
            Удалить
          </Button>
          <Button
            variant="contained"
            color="primary"
            onClick={handleDeletePopoverClose}
          >
            Отмена
          </Button>
        </div>
      </div>
    </Popover>
  );

  const errorPopover = (
    <Popover
      id="errorPopover"
      open={openError}
      anchorEl={anchorErrorEl}
      onClose={handleErrorPopoverClose}
      anchorOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
      transformOrigin={{
        vertical: 'center',
        horizontal: 'center',
      }}
    >
      <div className={classes.container}>
        <Typography>
          {error}
        </Typography>
      </div>
    </Popover>
  );

  const prettifyDate = () => {
    if (params.birthday) {
      const date = new Date(params.birthday);
      return date.toLocaleString('ru-RU', { year: 'numeric', month: 'numeric', day: 'numeric' });
    }
    return null;
  };

  return (
    <>
      <AppBar position="fixed">
        <Toolbar className={classes.toolbar}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleBackClick}
          >
            <BackIcon />
          </IconButton>
          {isStaff && (
            <div>
              <IconButton
                color="inherit"
                onClick={handleEditClick}
              >
                <EditIcon />
              </IconButton>
              <IconButton
                color="inherit"
                onClick={handleDeleteClick}
              >
                <DeleteIcon />
              </IconButton>
            </div>
          )}
        </Toolbar>
      </AppBar>
      <Container maxWidth="xl">
        <div className={classes.container}>
          <div className={classes.base} />
          <Typography variant="h5">
            {params.fullname}
          </Typography>
          {params.image && (
            <img src={params.image_url} alt={params.fullname} className={classes.image} />
          )}
          {prettifyDate() && (
            <Typography variant="subtitle1">
              {`Дата рождения: ${prettifyDate()}`}
            </Typography>
          )}
          <Typography variant="subtitle1">
            Кафедры, на которых работает преподаватель:
          </Typography>
          <div className={classes.container}>
            {
              params.department_details.map((detailes) => (
                <Typography variant="subtitle1" key={`department${detailes.id}`}>
                  {`${detailes.number}. ${detailes.name}`}
                </Typography>
              ))
            }
          </div>
          <div className={classes.assessment}>
            <div className={classes.starsContainer}>
              <Typography variant="subtitle1">
                Характер
              </Typography>
              <Typography variant="subtitle2">
                {params.rating_ch_avg && params.rating_ch_num
                && `Среднее - ${params.rating_ch_avg} (Проголосовало ${params.rating_ch_num})`}
              </Typography>
              <StarRatingComponent
                name="character-rating"
                starCount={10}
                value={characterRating}
                onStarClick={onCharacterStarClick}
                editing={!chDisabled}
              />
              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={onCharacterStarButtonClick}
                disabled={chDisabled}
              >
                Оценить характер
              </Button>
            </div>
            <div className={classes.starsContainer}>
              <Typography variant="subtitle1">
                Качество преподавания
              </Typography>
              <Typography variant="subtitle2">
                {params.rating_qu_avg && params.rating_qu_num
                && `Среднее - ${params.rating_qu_avg} (Проголосовало ${params.rating_qu_num})`}
              </Typography>
              <StarRatingComponent
                name="quality"
                starCount={10}
                value={qualityRating}
                onStarClick={onQualityStarClick}
                editing={!quDisabled}
              />
              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={onQualityStarButtonClick}
                disabled={quDisabled}
              >
                Оценить качество преподавания
              </Button>
            </div>
            <div className={classes.starsContainer}>
              <Typography variant="subtitle1">
                Прием зачетов и экзаменов
              </Typography>
              <Typography variant="subtitle2">
                {params.rating_ex_avg && params.rating_ex_num
                && `Среднее - ${params.rating_ex_avg} (Проголосовало ${params.rating_ex_num})`}
              </Typography>
              <StarRatingComponent
                name="exam-rating"
                starCount={10}
                value={examRating}
                onStarClick={onExamStarClick}
                editing={!exDisabled}
              />
              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={onExamStarButtonClick}
                disabled={exDisabled}
              >
                Оценить прием экзаменов
              </Button>
            </div>
          </div>
          <Typography variant="subtitle1">
            Дисциплины, которые ведет преподаватель:
          </Typography>
          <SubjectList
            list={params.subject_details}
            handleDelete={EditSubjectList}
            slug={params.slug}
          />
          <Popover
            id={idPopover}
            open={open}
            anchorEl={anchorEl}
            onClose={handlePopoverClose}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'center',
            }}
            transformOrigin={{
              vertical: 'top',
              horizontal: 'center',
            }}
          >
            <ProfessorForm
              mode="edit"
              slug={slug}
              firstName={firstName}
              lastName={lastName}
              patronimic={patronymic}
              birthday={params.birthday}
              selectedDepartments={params.department_details}
              selectedSubjects={params.subject_details}
              handlePopoverClose={handlePopoverClose}
              onEditHandle={setParams}
            />
          </Popover>
          {deletePopover}
          {errorPopover}
          <Comments slug={slug} subtitle="Отзывы о преподавателе" />
        </div>
      </Container>
    </>
  );
}
