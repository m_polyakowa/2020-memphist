import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import TopProfessors from '../components/ProfessorTop';
// Исходник https://material-ui.com/ru/getting-started/templates/

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

export default function Main() {
  const classes = useStyles();
  // eslint-disable-next-line
  const [top, setTop] = useState<JSX.Element | null>(null);

  useEffect(() => {
    (async () => {
      const component = await TopProfessors();
      setTop(component);
    })();
  }, []);

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        {top}
      </div>
    </Container>
  );
}
