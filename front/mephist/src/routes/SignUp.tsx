import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import yup from '../validate';
import { signUp, SignUpResError } from '../api/auth';
// Исходник https://material-ui.com/ru/getting-started/templates/

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  error: {
    color: 'red',
  },
}));

type BackendError = SignUpResError & { common?: string }

interface Form {
  email: string,
  username: string,
  password: string,
  confirmPassword: string,
}

const schema = yup.object().shape({
  email: yup.string().required().email(),
  username: yup.string().required(),
  password: yup.string().required().min(8),
  confirmPassword: yup.string().oneOf([yup.ref('password'), null], 'Пароли не совпадают'),
});

export default function SignUp() {
  const classes = useStyles();
  const [isActivateStep, setActiveStep] = useState(false);
  const [serverErrors, setError] = useState<BackendError>({});

  const { register, handleSubmit, errors } = useForm<Form>({
    resolver: yupResolver(schema),
    mode: 'onBlur',
  });

  const login = async (data: Form) => {
    const { email, username, password } = data;

    const res = await signUp({
      email,
      username,
      password,
    });

    console.log(typeof res.data);

    if (res.state === 'success') {
      setActiveStep(true);
    } else if (res.data) {
      setError({ ...res.data });
    } else {
      setError({ common: 'Что-то пошло не так ' });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      {!isActivateStep ? (
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Регистрация
          </Typography>
          <form className={classes.form} noValidate onSubmit={handleSubmit(login)}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Почта"
              name="email"
              autoComplete="email"
              autoFocus
              error={!!errors.email?.message || !!serverErrors.email}
              helperText={errors.email?.message || serverErrors.email}
              inputRef={register}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              label="Имя пользователя"
              name="username"
              autoComplete="username"
              error={!!errors.username?.message || !!serverErrors.username}
              helperText={errors.username?.message || serverErrors.username}
              inputRef={register}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Пароль"
              type="password"
              error={!!errors.password?.message || !!serverErrors.password}
              helperText={errors.password?.message || serverErrors.password}
              autoComplete="current-password"
              inputRef={register}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="confirmPassword"
              label="Подтвердите пароль"
              type="password"
              error={!!errors.confirmPassword?.message}
              helperText={errors.confirmPassword?.message}
              autoComplete="current-password"
              inputRef={register}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Зарегистрироваться
            </Button>
            <div className={classes.error}>
              {serverErrors.common}
            </div>
          </form>
        </div>
      ) : (
        <div className={classes.paper}>
          <Typography component="h1" variant="h5">
            Подтвердите почту по адресу
            {' '}
            <a href="https://mail.campus.mephi.ru/" target="__blank">https://mail.campus.mephi.ru/</a>
          </Typography>
        </div>
      )}
    </Container>
  );
}
