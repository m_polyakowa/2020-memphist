import React, { useEffect, useState } from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useParams } from 'react-router-dom';
import { activate, ActivateRequest } from '../api/auth';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
}));

export default function Activate() {
  const classes = useStyles();
  const params = useParams<ActivateRequest>();
  const [error, setError] = useState<string>('');

  useEffect(() => {
    async function fetchMyAPI() {
      const res = await activate({
        uidb64: params.uidb64,
        token: params.token,
      });
      if (res.state === 'failed' && res.data) {
        if (res.status === 403) {
          setError('Почта уже активирована');
        } else if (res.status === 408) {
          setError('Токен истёк');
        } else {
          setError('Что-то пошло не так');
        }
      }
    }

    fetchMyAPI();
  }, []);

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          { error === '' ? 'Почта успешно активирована' : error }
        </Typography>
      </div>
    </Container>
  );
}
