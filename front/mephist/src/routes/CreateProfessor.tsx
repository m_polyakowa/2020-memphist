import React from 'react';
import { useHistory, useLocation } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import BackIcon from '@material-ui/icons/ArrowBack';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { LocationState } from '../store/location';
import ProfessorForm from '../components/ProfessorForm';
import { useAuth } from '../components/auth/AuthProvider';

// https://material-ui.com/ru/components/autocomplete/

export interface Fields {
  lastName: string,
  firstName: string,
  patronymic?: string,
  birthday?: Date | null,
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  popover: {
    padding: 20,
  },

}));

const CreateProfessorForm: React.FC = () => {
  const location = useLocation<LocationState>();
  const history = useHistory();
  const { from } = location.state || { from: { pathname: '/' } };
  const { user } = useAuth();
  const isStaff = user?.is_staff;
  const classes = useStyles();

  const handleBackClick = () => {
    history.replace(from);
  };

  const form = (
    <>
      <Typography component="h1" variant="h5" align="center">
        Введите данные о преподавателе
      </Typography>
      <ProfessorForm
        mode="create"
      />
    </>
  );

  const error403 = (
    <Typography component="h1" variant="h5" align="center">
      К сожалению, у вас нет прав просматривать эту страницу
    </Typography>
  );

  return (
    <>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleBackClick}
          >
            <BackIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <Container maxWidth="xs">
        <div className={classes.toolbar} />
        {isStaff ? form : error403}
      </Container>
    </>
  );
};

export default CreateProfessorForm;
