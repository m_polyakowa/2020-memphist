import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory, useLocation } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import yup from '../validate';
import { useAuth } from '../components/auth/AuthProvider';
import { auth as authReq, AuthResError } from '../api/auth';
import { LocationState } from '../store/location';

// Исходник https://material-ui.com/ru/getting-started/templates/

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  error: {
    color: 'red',
  },
}));

type BackendError = AuthResError & { common?: string }
interface Form {
  email: string,
  password: string,
}

const schema = yup.object().shape({
  email: yup.string().required().email(),
  password: yup.string().required(),
});

export default function Login() {
  const classes = useStyles();
  const auth = useAuth();
  const location = useLocation<LocationState>();
  const history = useHistory();
  const [serverErrors, setError] = useState<BackendError>({});

  const { register, handleSubmit, errors } = useForm<Form>({
    resolver: yupResolver(schema),
    mode: 'onBlur',
  });

  const { from } = location.state || { from: { pathname: '/' } };
  const login = async (data: Form) => {
    const { email, password } = data;

    const res = await authReq({
      email,
      password,
    });

    if (res.state === 'success') {
      auth.signin({ ...res.data }, () => {
        history.replace(from);
      });
    } else if (res.data) {
      setError({ ...res.data });
    } else if (res.status === 404) {
      setError({ common: 'Пользователь не найден' });
    } else {
      setError({ common: 'Что-то пошло не так ' });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Войти
        </Typography>
        <form className={classes.form} noValidate onSubmit={handleSubmit(login)}>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Почта"
            name="email"
            autoComplete="email"
            autoFocus
            error={!!errors.email?.message || !!serverErrors.email}
            helperText={errors.email?.message || serverErrors.email}
            inputRef={register}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Пароль"
            type="password"
            id="password"
            autoComplete="current-password"
            error={!!errors.password?.message || !!serverErrors.password}
            helperText={errors.password?.message || serverErrors.password}
            inputRef={register}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Войти
          </Button>
          <div className={classes.error}>
            {serverErrors.common}
          </div>
        </form>
      </div>
    </Container>
  );
}
