import json
import re
import sys
from collections import defaultdict
from typing import Optional, Union
from urllib.parse import urlencode
from urllib.request import Request, urlopen


PROJECT_ID = 21693221


def request(url: str, params: dict = {}, method: str = None) -> Union[dict, list]:
    headers = {'PRIVATE-TOKEN': sys.argv[1]}
    request = Request(url, headers=headers, method=method)
    if params:
        request.data = urlencode(params).encode()
    decoded = urlopen(request).read().decode()
    return json.loads(decoded)


def get_system_comment(issue_id: str) -> Optional[dict]:
    url = f'https://gitlab.com/api/v4/projects/{PROJECT_ID}/issues/{issue_id}/notes'
    json = request(url)
    comment = next((
        data for data in json
        if data['created_at'] == '1970-01-01T00:00:00.000Z'
    ), None)
    return comment


def parse_line(line: str) -> list[str]:
    regex = r'FR-[\d.]*'
    fr = re.search(regex, line).group()

    regex = r'2020-memphist\/-\/issues\/(\d+)'
    issues = re.findall(regex, line)
    return fr, issues


def create_comment(frs: list[str]) -> str:
    header = 'Данная задача покрывает следующие требования: '

    links = []
    for fr in frs:
        fr_stripped = fr.lower().replace('.', '')
        links.append(
            f'[{fr}](https://gitlab.com/m_polyakowa/2020-memphist/-/blob/master/docs/'
            f'Требования/Функциональные%20требования.md#{fr_stripped})'
        )
    return header + ', '.join(links)


tracing = defaultdict(list)
with open('docs/Трассировка/Функциональные требования-код.md', 'r') as f:
    for line in f.readlines():
        if '2020-memphist/-/issues' in line:
            fr, issues = parse_line(line)
            for issue in issues:
                tracing[issue].append(fr)

for issue in tracing:
    url = f'https://gitlab.com/api/v4/projects/{PROJECT_ID}/issues/{issue}/notes'
    body = create_comment(tracing[issue])
    data = {
        'body': body,
        'created_at': '1970-01-01',
    }
    if (comment := get_system_comment(issue)):
        url += f'/{comment["id"]}'
        request(url, params=data, method='PUT')
    else:
        request(url, params=data)
