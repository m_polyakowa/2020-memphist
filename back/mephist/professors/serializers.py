import datetime

from departments.serializers import DepartmentSerializer
from django.contrib.contenttypes.models import ContentType
from django.db.models import Avg
from main.serializers import UserSerializer
from rating.models import Rating
from rest_framework import serializers
from subjects.serializers import SubjectSerializer


from .models import Feedback, Professor


def get_rating(professor_id: int, rating_type: str) -> tuple[float, int]:
    content_type = ContentType.objects.get_for_model(
        Rating.TYPES[rating_type]['model'],
    ).id
    count = Rating.objects.filter(
        content_type=content_type, content_id=professor_id, rating_type=rating_type,
    ).count()
    average = Rating.objects.filter(
        content_type=content_type, content_id=professor_id, rating_type=rating_type,
    ).aggregate(Avg('value'))['value__avg']
    return average, count


class ProfessorSerializer(serializers.ModelSerializer):
    department_details = DepartmentSerializer(read_only=True, many=True, source='departments')
    subject_details = SubjectSerializer(read_only=True, many=True, source='subjects')
    image_url = serializers.SerializerMethodField()

    rating_ch_avg = serializers.SerializerMethodField()
    rating_ch_num = serializers.SerializerMethodField()
    rating_qu_avg = serializers.SerializerMethodField()
    rating_qu_num = serializers.SerializerMethodField()
    rating_ex_avg = serializers.SerializerMethodField()
    rating_ex_num = serializers.SerializerMethodField()

    class Meta:
        model = Professor
        fields = [
            'slug', 'image', 'image_url', 'fullname', 'birthday',
            'rating_ch_avg', 'rating_qu_avg', 'rating_ex_avg',
            'rating_ch_num', 'rating_qu_num', 'rating_ex_num',
            'departments', 'department_details',
            'subjects', 'subject_details', 'id',
        ]

    def get_image_url(self, obj: Professor) -> str:
        request = self.context.get('request')
        if not obj.image:
            return ''
        image_url = obj.image.image.url
        return request.build_absolute_uri(image_url)

    def get_rating_ch_avg(self, obj: Professor) -> float:
        return get_rating(obj.id, 'ch')[0]

    def get_rating_ch_num(self, obj: Professor) -> int:
        return get_rating(obj.id, 'ch')[1]

    def get_rating_qu_avg(self, obj: Professor) -> float:
        return get_rating(obj.id, 'qu')[0]

    def get_rating_qu_num(self, obj: Professor) -> int:
        return get_rating(obj.id, 'qu')[1]

    def get_rating_ex_avg(self, obj: Professor) -> float:
        return get_rating(obj.id, 'ex')[0]

    def get_rating_ex_num(self, obj: Professor) -> int:
        return get_rating(obj.id, 'ex')[1]

    def validate_birthday(self, value: datetime.date) -> datetime.date:
        if not value:
            return value
        today = datetime.date.today()
        age = (
            today.year - value.year
            - ((today.month, today.day) < (value.month, value.day))
        )
        if not age >= 18:
            raise serializers.ValidationError('Professor must be older than 17 years old')
        return value


class FeedbackSerializer(serializers.ModelSerializer):
    user_details = UserSerializer(read_only=True, source='user')
    professor_details = ProfessorSerializer(read_only=True, source='professor')

    class Meta:
        model = Feedback
        fields = ['id', 'user', 'user_details', 'professor', 'professor_details', 'title', 'date_created', 'content']
