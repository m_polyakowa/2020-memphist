from django.contrib import admin

from .models import Feedback, Professor

# Register your models here.
admin.site.register(Professor)
admin.site.register(Feedback)
