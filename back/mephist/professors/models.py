from departments.models import Department
from django.db import models
from django.utils import timezone
from images.models import Image
from main.models import User
from subjects.models import Subject


class Professor(models.Model):
    class Meta:
        db_table = 'Professor'
        verbose_name = 'Professor'
        ordering = ['slug']

    # for each type you must add 2 fields: _avg and _num
    RATING_TYPES = ('rating_ch', 'rating_qu', 'rating_ex')

    image = models.OneToOneField(Image, on_delete=models.SET_NULL, blank=True, null=True, related_name='professor')
    fullname = models.CharField(max_length=100, verbose_name='Полное имя преподавателя')
    departments = models.ManyToManyField(Department, related_name='professors')
    subjects = models.ManyToManyField(Subject, related_name='professors')
    birthday = models.DateField(verbose_name='Дата рождения', null=True, blank=True, default=None)
    slug = models.SlugField(max_length=50, unique=True)

    def __str__(self) -> str:
        return self.fullname


class Feedback(models.Model):
    class Meta:
        db_table = 'Feedback'
        verbose_name = 'Feedback'
        ordering = ['-date_created']

    user = models.ForeignKey(User, on_delete=models.CASCADE, db_index=False)
    professor = models.ForeignKey(Professor, on_delete=models.CASCADE, db_index=False)
    title = models.CharField(verbose_name='Название', max_length=100)
    date_created = models.DateTimeField(default=timezone.now)
    content = models.TextField(verbose_name='Содержание')

    def __str__(self) -> str:
        return self.title
