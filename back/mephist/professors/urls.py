from django.urls import path

from .views import (
    add_feedback, add_professor, artifacts, feedback_detail, list_feedbacks,
    list_professors, professor_detail, top_best_professors, top_worst_professors
)

urlpatterns = [
    path('', list_professors),
    path('add_professor', add_professor),
    path('top_best_professors', top_best_professors),
    path('top_worst_professors', top_worst_professors),
    path('add_feedback', add_feedback),
    path('feedback/<id>', feedback_detail),
    path('<slug>', professor_detail),
    path('<slug>/list_feedbacks', list_feedbacks),
    path('<slug>/<subject>', artifacts),
]
