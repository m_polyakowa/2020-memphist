from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import get_object_or_404
from rest_framework.permissions import BasePermission

from .models import Feedback


class IsOwner(BasePermission):
    def has_permission(self, request: WSGIRequest, view: object) -> bool:
        if str(view.__dict__['request']).find('feedback') != -1:
            feedback = get_object_or_404(Feedback, id=view.kwargs['id'])
            is_owner = (feedback.user_id == request.user.id)
        return bool(request.user and is_owner)
