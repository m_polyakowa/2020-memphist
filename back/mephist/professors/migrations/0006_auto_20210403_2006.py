# Generated by Django 3.1.7 on 2021-04-03 20:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('departments', '0001_initial'),
        ('professors', '0005_auto_20210403_1745'),
    ]

    operations = [
        migrations.AlterField(
            model_name='professor',
            name='departments',
            field=models.ManyToManyField(related_name='professors', to='departments.Department'),
        ),
    ]
