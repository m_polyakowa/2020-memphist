# Generated by Django 3.1.7 on 2021-04-09 18:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('professors', '0007_auto_20210404_1321'),
    ]

    operations = [
        migrations.AlterField(
            model_name='professor',
            name='slug',
            field=models.SlugField(unique=True),
        ),
    ]
