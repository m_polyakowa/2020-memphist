import unidecode

from django.core.handlers.wsgi import WSGIRequest
from django.db.models import Avg, F, Q
from django.shortcuts import get_object_or_404
from django.utils.text import slugify as slugify
from rating.models import Rating
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND,
)
from subject_artifacts.models import SubjectArtifact

from .models import Feedback, Professor
from .permissions import IsOwner
from .serializers import FeedbackSerializer, ProfessorSerializer


def _create_slug(fullname: str, existing_slug: str = '') -> str:
    latin_name = unidecode.unidecode(fullname).split()
    slug = latin_name[0] + ''.join([x[0] for x in latin_name[1:]])
    slug = slugify(slug)
    if slug in existing_slug:  # For PUT - check if we need to update the slug
        return existing_slug
    namesakes = Professor.objects.filter(slug__startswith=slug)
    postfix = 0
    for namesake in namesakes:
        try:
            num = int(''.join(filter(str.isdigit, namesake.slug)))
        except ValueError:
            num = 1
        postfix = max(num, postfix)

    if postfix:
        slug += f'{postfix+1}'
    return slug


@api_view(['POST'])
@permission_classes([IsAdminUser])
def add_professor(request: WSGIRequest) -> Response:
    data = request.data.copy()
    data['slug'] = _create_slug(data['fullname'])
    serialized = ProfessorSerializer(data=data, context={'request': request})
    if serialized.is_valid():
        serialized.save()
        return Response(serialized.data, status=HTTP_201_CREATED)
    return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def list_professors(request: WSGIRequest) -> Response:
    data = request.data
    n = data.get('n', 100000)  # we won't have 100000 professors, right?
    last_id = data.get('last_id', '')
    if (professors := Professor.objects.filter(slug__gt=last_id)[:n].values('slug', 'fullname')):
        return Response(professors, status=HTTP_200_OK)
    return Response([], status=HTTP_204_NO_CONTENT)


@api_view(['GET', 'DELETE', 'PUT'])
def professor_detail(request: WSGIRequest, slug: str) -> Response:
    try:
        professor = Professor.objects.get(slug=slug)
    except Professor.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serialized = ProfessorSerializer(professor, context={'request': request})
        return Response(serialized.data, status=HTTP_200_OK)

    if request.method == 'DELETE':
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        professor.delete()
        return Response(status=HTTP_204_NO_CONTENT)

    if request.method == 'PUT':
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        data = request.data
        data['slug'] = _create_slug(data['fullname'], data['slug'])
        serialized = ProfessorSerializer(professor, data=data, context={'request': request})
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=HTTP_200_OK)
        return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def artifacts(request: WSGIRequest, slug: str, subject: int) -> Response:
    if (artifacts := SubjectArtifact.objects.filter(professor__slug=slug, subject=subject)):
        return Response(artifacts.values('id', 'name', 'date_created', 'file_hash'), status=HTTP_200_OK)
    return Response([], status=HTTP_204_NO_CONTENT)


@api_view(['GET'])
def top_best_professors(request: WSGIRequest) -> Response:
    ch_avg = Avg('value', filter=Q(rating_type__exact='ch'))
    qu_avg = Avg('value', filter=Q(rating_type__exact='qu'))
    ex_avg = Avg('value', filter=Q(rating_type__exact='ex'))
    top = Rating.objects.values('content_id').annotate(
        avg_rating=3 / (1 / ch_avg + 1 / ex_avg + 1 / qu_avg),
    ).order_by(F('avg_rating').desc(nulls_last=True))[:10]
    result = []
    for row in top:
        professor = Professor.objects.get(id=row['content_id'])
        result.append({
            'slug': professor.slug,
            'fullname': professor.fullname,
            'avg_rating': row['avg_rating'],
        })
    return Response(data=result, status=HTTP_200_OK)


@api_view(['GET'])
def top_worst_professors(request: WSGIRequest) -> Response:
    ch_avg = Avg('value', filter=Q(rating_type__exact='ch'))
    qu_avg = Avg('value', filter=Q(rating_type__exact='qu'))
    ex_avg = Avg('value', filter=Q(rating_type__exact='ex'))
    top = Rating.objects.values('content_id').annotate(
        avg_rating=3 / (1 / ch_avg + 1 / ex_avg + 1 / qu_avg),
    ).order_by(F('avg_rating').asc(nulls_last=True))[:10]
    result = []
    for row in top:
        professor = Professor.objects.get(id=row['content_id'])
        result.append({
            'slug': professor.slug,
            'fullname': professor.fullname,
            'avg_rating': row['avg_rating'],
        })
    return Response(data=result, status=HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_feedback(request: WSGIRequest) -> Response:
    data = dict([(key, request.data.get(key, None)) for key in ['title', 'content']])
    data['user'] = request.user.id
    professor = get_object_or_404(Professor, slug=request.data.get('slug', ''))
    data['professor'] = professor.id
    serialized = FeedbackSerializer(data=data)
    if serialized.is_valid():
        serialized.save()
        feedback = {
            'id': serialized.data['id'], 'username': serialized.data['user_details']['username'],
            'title': serialized.data['title'], 'content': serialized.data['content'],
            'datetime': serialized.data['date_created'],
        }
        return Response(feedback, status=HTTP_201_CREATED)
    else:
        return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def list_feedbacks(request: WSGIRequest, slug: str) -> Response:
    professor = get_object_or_404(Professor, slug=slug)
    feedbacks = Feedback.objects.filter(professor=professor.id)
    feedbacks = [
        {
            'id': i.id, 'username': i.user.username, 'title': i.title, 'content': i.content,
            'datetime': i.date_created,
        }
        for i in feedbacks
    ]
    return Response(feedbacks, status=HTTP_200_OK)


@api_view(['DELETE', 'PATCH'])
@permission_classes([IsAdminUser | IsOwner])
def feedback_detail(request: WSGIRequest, id: int) -> Response:
    feedback = get_object_or_404(Feedback, id=id)

    if request.method == 'DELETE':
        feedback.delete()
        return Response(status=HTTP_200_OK)

    if request.method == 'PATCH':
        data = dict()
        if request.data.get('title', '') != '':
            data['title'] = request.data['title']
        if request.data.get('content', '') != '':
            data['content'] = request.data['content']
        serialized = FeedbackSerializer(feedback, data=data, partial=True)
        if serialized.is_valid():
            serialized.save()
            feedback = {
                'id': serialized.data['id'], 'username': serialized.data['user_details']['username'],
                'title': serialized.data['title'], 'content': serialized.data['content'],
                'datetime': serialized.data['date_created'],
            }
            return Response(feedback, status=HTTP_200_OK)
        else:
            return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)
