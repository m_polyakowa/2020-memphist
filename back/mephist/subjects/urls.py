from django.urls import path

from .views import add_subject, list_subjects, subject_detail

urlpatterns = [
    path('', list_subjects),
    path('add_subject', add_subject),
    path('<pk>', subject_detail)
]
