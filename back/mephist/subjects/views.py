from django.core.handlers.wsgi import WSGIRequest
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND,
)

from .models import Subject
from .serializers import SubjectSerializer


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_subject(request: WSGIRequest) -> Response:
    serialized = SubjectSerializer(data=request.data)
    if serialized.is_valid():
        serialized.save()
        return Response(serialized.data, status=HTTP_201_CREATED)
    return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def list_subjects(request: WSGIRequest) -> Response:
    subjects = Subject.objects.all()
    if not subjects:
        return Response([], status=HTTP_204_NO_CONTENT)
    serialized = SubjectSerializer(subjects, many=True)
    return Response(serialized.data, status=HTTP_200_OK)


@api_view(['GET', 'PUT', 'DELETE'])
def subject_detail(request: WSGIRequest, pk: int) -> Response:
    try:
        subject = Subject.objects.get(pk=pk)
    except Subject.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serialized = SubjectSerializer(subject)
        return Response(serialized.data, status=HTTP_200_OK)

    if request.method == 'DELETE':
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        subject.delete()
        return Response(status=HTTP_204_NO_CONTENT)

    if request.method == 'PUT':
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        serialized = SubjectSerializer(subject, data=request.data)
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=HTTP_200_OK)
        return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)
