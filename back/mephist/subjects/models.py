from django.db import models


class Subject(models.Model):
    class Meta:
        db_table = 'Subject'
        verbose_name = 'Subject'
        ordering = ['name']

    name = models.CharField(max_length=200, verbose_name='Учебная дисциплина', unique=True)
    description = models.TextField(verbose_name='Описание учебной дисциплины', null=True, blank=True, default=None)
