# Generated by Django 3.1.7 on 2021-04-14 19:35

from django.db import migrations, models
import django.db.models.deletion
import subject_artifacts.models


class Migration(migrations.Migration):

    dependencies = [
        ('professors', '0009_professor_subjects'),
        ('subjects', '0002_auto_20210409_1857'),
        ('subject_artifacts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='subjectartifact',
            name='name',
            field=models.CharField(default='test', max_length=150, verbose_name='Название файла'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='subjectartifact',
            name='professor',
            field=models.ForeignKey(default=30, on_delete=django.db.models.deletion.CASCADE, related_name='artifacts', to='professors.professor'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='subjectartifact',
            name='subject',
            field=models.ForeignKey(default=4, on_delete=django.db.models.deletion.CASCADE, related_name='artifacts', to='subjects.subject'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='subjectartifact',
            name='artifact',
            field=models.FileField(upload_to=subject_artifacts.models.destination),
        ),
    ]
