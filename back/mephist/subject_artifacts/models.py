from django.db import models
from professors.models import Professor
from subjects.models import Subject


def destination(instance: 'SubjectArtifact', filename: str) -> str:
    return instance.file_hash


class SubjectArtifact(models.Model):
    ALLOWED_EXTENSIONS = ['pdf', 'rar', 'zip', '7z', 'tar', 'gz', 'bz2']

    class Meta:
        db_table = 'SubjectArtifact'
        verbose_name = 'SubjectArtifact'
        ordering = ['-date_created']

    professor = models.ForeignKey(Professor, related_name='artifacts', on_delete=models.CASCADE)
    subject = models.ForeignKey(Subject, related_name='artifacts', on_delete=models.CASCADE)
    name = models.CharField(max_length=150, verbose_name='Название файла')
    artifact = models.FileField(upload_to=destination)
    date_created = models.DateTimeField(verbose_name='Дата загрузки', auto_now_add=True, unique=True)
    file_hash = models.CharField(verbose_name='sha256 хеш', max_length=64, unique=True)

    def delete(self, *args: tuple, **kwargs: dict) -> tuple:
        self.artifact.delete()
        super().delete(*args, **kwargs)
