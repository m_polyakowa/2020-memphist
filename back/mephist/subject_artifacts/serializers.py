import filetype

from django.core.files.uploadedfile import InMemoryUploadedFile
from rest_framework import serializers

from .models import SubjectArtifact


class SubjectArtifactSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubjectArtifact
        fields = '__all__'

    def validate_artifact(self, value: InMemoryUploadedFile) -> InMemoryUploadedFile:
        kind = filetype.guess(value.read(300))
        if kind and kind.extension.lower() in SubjectArtifact.ALLOWED_EXTENSIONS:
            value.seek(0)
            return value
        raise serializers.ValidationError(
            'File type is not supported. Supported filetypes are: ' + ', '.join(SubjectArtifact.ALLOWED_EXTENSIONS)
        )
