from django.apps import AppConfig


class SubjectArtifactsConfig(AppConfig):
    name = 'subject_artifacts'
