import filetype

from django.core.handlers.wsgi import WSGIRequest
from django.http import FileResponse, Http404
from mephist.utils import calculate_sha256
from professors.models import Professor
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN
from rest_framework.views import APIView

from .models import SubjectArtifact
from .serializers import SubjectArtifactSerializer


class SubjectArtifactUpload(APIView):
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [IsAuthenticated]

    def post(self, request: WSGIRequest) -> Response:
        request.data['file_hash'] = calculate_sha256(request.FILES['artifact'])
        request.data['professor'] = Professor.objects.get(slug=request.data['professor']).pk

        serialized = SubjectArtifactSerializer(data=request.data)
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=HTTP_201_CREATED)
        else:
            return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


class SubjectArtifactDetail(APIView):

    def get_object(self, pk: int) -> SubjectArtifact:
        try:
            return SubjectArtifact.objects.get(pk=pk)
        except SubjectArtifact.DoesNotExist:
            raise Http404

    def get(self, request: WSGIRequest, pk: int) -> FileResponse:
        subject_artifact = self.get_object(pk)
        path = subject_artifact.artifact.path
        name = subject_artifact.name
        with open(path, 'rb') as f:
            kind = filetype.guess(f.read(300))
        file_resp = FileResponse(open(path, 'rb'), filename=f'{name}.{kind.extension}')
        file_resp['Content-Type'] = kind.mime
        return file_resp

    def delete(self, request: WSGIRequest, pk: int) -> Response:
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        subject_artifact = self.get_object(pk)
        subject_artifact.delete()
        return Response(status=HTTP_204_NO_CONTENT)
