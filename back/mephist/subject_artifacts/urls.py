from django.urls import path

from .views import SubjectArtifactDetail, SubjectArtifactUpload

urlpatterns = [
    path('upload', SubjectArtifactUpload.as_view()),
    path('<pk>', SubjectArtifactDetail.as_view()),
]
