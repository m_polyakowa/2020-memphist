import hashlib

from django.core.files.uploadedfile import InMemoryUploadedFile


def calculate_sha256(file: InMemoryUploadedFile) -> str:
    sha256_hash = hashlib.sha256()
    for chunk in iter(lambda: file.read(4096), b''):
        sha256_hash.update(chunk)
    file.seek(0)
    return sha256_hash.hexdigest()
