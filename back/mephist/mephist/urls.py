from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('account/', include('main.urls')),
    path('departments/', include('departments.urls')),
    path('images/', include('images.urls')),
    path('professors/', include('professors.urls')),
    path('rating/', include('rating.urls')),
    path('subjects/', include('subjects.urls')),
    path('subject_artifacts/', include('subject_artifacts.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
