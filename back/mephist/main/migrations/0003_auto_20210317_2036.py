# Generated by Django 3.1.7 on 2021-03-17 20:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20210317_1901'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='is_moderator',
            new_name='is_staff',
        ),
    ]
