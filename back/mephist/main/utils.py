from django.contrib.auth.tokens import PasswordResetTokenGenerator
from main.models import User
from six import text_type


class ActivationTokenGenerator(PasswordResetTokenGenerator):

    def _make_hash_value(self, user: User, timestamp: int) -> str:
        return text_type(user.pk) + text_type(timestamp) + text_type(user.is_active)


account_activation_token = ActivationTokenGenerator()
