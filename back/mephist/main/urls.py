# from django.contrib import admin
from django.urls import path

from .views import activate, get_data, login_user, logout_user, signup

urlpatterns = [
    path('signup', signup),
    path('activate/<uidb64>/<token>', activate, name='activate'),
    path('login', login_user),
    path('logout', logout_user),
    path('getdata', get_data)
]
