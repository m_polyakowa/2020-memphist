from django.contrib.auth import authenticate, login, logout
from django.core.handlers.wsgi import WSGIRequest
from django.core.mail import EmailMessage
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND, HTTP_408_REQUEST_TIMEOUT
)

from .models import User
from .permissions import IsNotAuthenticated
from .serializers import UserSerializer
from .utils import account_activation_token


def email_activator(user: User) -> None:
    email_subject = 'Подтвердите вашу почту'
    domain = 'localhost:3000'
    uidb64 = urlsafe_base64_encode(force_bytes(user.id))
    token = account_activation_token.make_token(user)
    link = reverse('activate', kwargs={
        'uidb64': uidb64,
        'token': token
    })
    absurl = 'http://' + domain + link
    email_body = 'Привет, ' + user.username + '. Используй следующую ссылку, чтобы подтвердить почту \n' + absurl
    email = EmailMessage(
        email_subject,
        email_body,
        'mephistm20@gmail.com',
        [user.email, ],
    )
    email.send(fail_silently=False)


@api_view(['POST'])
@permission_classes([IsNotAuthenticated])
def signup(request: WSGIRequest) -> Response:
    user = UserSerializer(data=request.data)
    if user.is_valid():
        user = user.save()
        user.save()
    else:
        return Response(user.errors, status=HTTP_400_BAD_REQUEST)
    email_activator(user)
    return Response(user.username, status=HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes([IsNotAuthenticated])
def activate(request: WSGIRequest, uidb64: str, token: str) -> Response:
    id = force_text(urlsafe_base64_decode(uidb64))
    user = get_object_or_404(User, id=id)
    if user.is_active:
        return Response(status=HTTP_403_FORBIDDEN)
    if not account_activation_token.check_token(user, token):
        return Response(status=HTTP_408_REQUEST_TIMEOUT)
    user.is_active = True
    user.save()
    return Response(status=HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsNotAuthenticated])
def login_user(request: WSGIRequest) -> Response:
    user = authenticate(
        request, email=request.data.get('email', ''), password=request.data.get('password', '')
    )
    if user is not None:
        login(request, user)
        return Response({'username': user.username, 'is_staff': user.is_staff}, status=HTTP_200_OK)
    else:
        return Response(status=HTTP_404_NOT_FOUND)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def logout_user(request: WSGIRequest) -> Response:
    logout(request)
    return Response(status=HTTP_200_OK)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_data(request: WSGIRequest) -> Response:
    return Response({'username': request.user.username, 'is_staff': request.user.is_staff}, status=HTTP_200_OK)
