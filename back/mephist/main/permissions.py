from django.core.handlers.wsgi import WSGIRequest
from rest_framework.permissions import BasePermission


class IsNotAuthenticated(BasePermission):
    def has_permission(self, request: WSGIRequest, view: object) -> bool:
        return bool(not request.user.is_authenticated)
