from re import match

from rest_framework import serializers

from .models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def validate_email(self, value: str) -> str:
        if match(r'[a-z]{3}\d{3}@campus.mephi.ru', value) is None or match(value, r'[a-z]{3}\d{3}@campus.mephi.ru'):
            raise serializers.ValidationError('Email must be mephist')
        else:
            return value

    def create(self, validated_data: dict) -> User:
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        return user
