from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import UserManager
from django.db import models
from django.utils import timezone


class User(AbstractBaseUser, PermissionsMixin):

    class Meta:
        db_table = 'User'
        verbose_name = 'User'

    username = models.CharField(verbose_name='Имя пользователя', max_length=30, unique=True)
    email = models.EmailField(verbose_name='Почта', unique=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'

    objects = UserManager()

    def __str__(self) -> str:
        return self.username
