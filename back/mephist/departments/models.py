from django.db import models


class Department(models.Model):
    class Meta:
        db_table = 'Department'
        verbose_name = 'Department'
        ordering = ['number']

    number = models.SmallIntegerField(verbose_name='Номер кафедры', unique=True)
    name = models.CharField(max_length=150, verbose_name='Название кафедры', unique=True)
    is_active = models.BooleanField(default=True)
