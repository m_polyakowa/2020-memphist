from rest_framework import serializers

from .models import Department


class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = '__all__'

    def validate_number(self, value: int) -> int:
        if value > 1000:
            raise serializers.ValidationError('Номер кафедры не может быть больше 1000')
        return value
