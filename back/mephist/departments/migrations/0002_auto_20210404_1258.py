# Generated by Django 3.1.7 on 2021-04-04 12:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('departments', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='department',
            old_name='no',
            new_name='number',
        ),
    ]
