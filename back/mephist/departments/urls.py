from django.urls import path

from .views import add_department, department_detail, list_departments

urlpatterns = [
    path('', list_departments),
    path('add_department', add_department),
    path('<number>', department_detail)
]
