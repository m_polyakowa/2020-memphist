from django.core.handlers.wsgi import WSGIRequest
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK, HTTP_201_CREATED, HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN, HTTP_404_NOT_FOUND,
)

from .models import Department
from .serializers import DepartmentSerializer


@api_view(['POST'])
@permission_classes([IsAdminUser])
def add_department(request: WSGIRequest) -> Response:
    serialized = DepartmentSerializer(data=request.data)
    if serialized.is_valid():
        serialized.save()
        return Response(serialized.data, status=HTTP_201_CREATED)
    return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def list_departments(request: WSGIRequest) -> Response:
    departments = Department.objects.all()
    if not departments:
        return Response([], status=HTTP_204_NO_CONTENT)
    serialized = DepartmentSerializer(departments, many=True)
    return Response(serialized.data, status=HTTP_200_OK)


@api_view(['GET', 'PUT', 'DELETE'])
def department_detail(request: WSGIRequest, number: int) -> Response:
    try:
        department = Department.objects.get(number=number)
    except Department.DoesNotExist:
        return Response(status=HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serialized = DepartmentSerializer(department)
        return Response(serialized.data, status=HTTP_200_OK)

    if request.method == 'DELETE':
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        department.delete()
        return Response(status=HTTP_204_NO_CONTENT)

    if request.method == 'PUT':
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        serialized = DepartmentSerializer(department, data=request.data)
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=HTTP_200_OK)
        return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)
