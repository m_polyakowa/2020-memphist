from django.contrib.contenttypes.models import ContentType
from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import get_object_or_404
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

from .models import Rating


class IsOwner(BasePermission):
    def has_permission(self, request: WSGIRequest, view: object) -> bool:
        if view.kwargs['user_type'] not in Rating.TYPES:
            return Response('Wrong type', status=HTTP_400_BAD_REQUEST)
        content_type = ContentType.objects.get_for_model(
            Rating.TYPES[view.kwargs['user_type']]['model'],
        ).id
        rating_type_ = Rating.TYPES[view.kwargs['user_type']]['type']
        rating_exists = bool(
            get_object_or_404(
                Rating, user=request.user.id, content_type=content_type,
                content_id=view.kwargs['id'], rating_type=rating_type_
            )
        )
        return bool(request.user and rating_exists)
