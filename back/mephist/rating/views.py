from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ObjectDoesNotExist
from django.core.handlers.wsgi import WSGIRequest
from django.db.models import Avg
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND

from .models import Rating
from .permissions import IsOwner
from .serializers import RatingSerializer


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def add_rating(request: WSGIRequest) -> Response:
    if request.data.get('user_type', '') not in Rating.TYPES:
        return Response('Wrong type', status=HTTP_400_BAD_REQUEST)

    # check object exists
    content_type = ContentType.objects.get_for_model(
        Rating.TYPES[request.data['user_type']]['model'],
    )
    try:
        content_type.get_object_for_this_type(id=request.data.get('content_id', -1))
    except ObjectDoesNotExist:
        return Response('Object does not exist', status=HTTP_404_NOT_FOUND)

    data = {
        'value': request.data.get('value', ''),
        'content_id': request.data.get('content_id', -1),
        'content_type': content_type.id,
        'rating_type': Rating.TYPES[request.data['user_type']]['type'],
        'user': request.user.id,
    }
    serialized = RatingSerializer(data=data)
    if serialized.is_valid():
        serialized.save()
        return Response(serialized.data, status=HTTP_201_CREATED)
    else:
        return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def get_rating(request: WSGIRequest, user_type: str, id: int) -> Response:
    if user_type not in Rating.TYPES:
        return Response('Wrong type', status=HTTP_400_BAD_REQUEST)
    content_type = ContentType.objects.get_for_model(
        Rating.TYPES[user_type]['model'],
    ).id
    rating_type = Rating.TYPES[user_type]['type']
    if rating_type == '__':
        likes = Rating.objects.filter(
            content_type=content_type, content_id=id, rating_type=rating_type, value=1,
        ).count()
        dislikes = Rating.objects.filter(
            content_type=content_type, content_id=id, rating_type=rating_type, value=-1,
        ).count()
        return Response({'likes': likes, 'dislikes': dislikes}, status=HTTP_200_OK)
    else:
        count = Rating.objects.filter(
            content_type=content_type, content_id=id, rating_type=rating_type,
        ).count()
        average = Rating.objects.filter(
            content_type=content_type, content_id=id, rating_type=rating_type,
        ).aggregate(Avg('value'))['value__avg']
        return Response({'count': count, 'average': average}, status=HTTP_200_OK)


@api_view(['GET', 'DELETE', 'PATCH'])
@permission_classes([IsAdminUser | IsOwner])
def rating_detail(request: WSGIRequest, user_type: str, id: int) -> Response:
    if user_type not in Rating.TYPES:
        return Response('Wrong type', status=HTTP_400_BAD_REQUEST)
    content_type = ContentType.objects.get_for_model(
        Rating.TYPES[user_type]['model'],
    ).id
    rating_type = Rating.TYPES[user_type]['type']
    rating = get_object_or_404(
        Rating, user=request.user.id, content_type=content_type, content_id=id, rating_type=rating_type
    )

    if request.method == 'GET':
        serialized = RatingSerializer(rating)
        return Response(serialized.data, status=HTTP_200_OK)

    if request.method == 'DELETE':
        rating.delete()
        return Response(status=HTTP_200_OK)

    if request.method == 'PATCH':
        serialized = RatingSerializer(
            rating,
            data={'value': request.data.get('value', -2), 'rating_type': rating.rating_type},
            partial=True,
        )
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=HTTP_201_CREATED)
        else:
            return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)
