from main.serializers import UserSerializer
from rest_framework import serializers

from .models import Rating


class RatingSerializer(serializers.ModelSerializer):
    user_details = UserSerializer(read_only=True, source='user')

    class Meta:
        model = Rating
        fields = '__all__'
        fields = ['user', 'user_details', 'content_type', 'content_id', 'rating_type', 'value']

    def validate_value(self, value: int) -> int:
        rating_type = self.initial_data['rating_type']
        if rating_type == '__':
            if value in [-1, 1]:
                return value
            else:
                raise serializers.ValidationError('Like must be in [-1, 1]')
        else:
            if value in range(1, 11):
                return value
            else:
                raise serializers.ValidationError('Rating must be in [1, 10]')
