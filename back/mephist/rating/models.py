from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from main.models import User
from professors.models import Feedback, Professor


class Rating(models.Model):
    class Meta:
        db_table = 'Rating'
        verbose_name = 'Rating'
        unique_together = ['user', 'content_type', 'content_id', 'rating_type']

    TYPES = {
        'ch': {'model': Professor, 'type': 'ch'},
        'qu': {'model': Professor, 'type': 'qu'},
        'ex': {'model': Professor, 'type': 'ex'},
        'fb': {'model': Feedback, 'type': '__'},
        # 'cm': {'model': Comment, 'type': '__'},
    }

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    content_id = models.PositiveIntegerField()
    rating_type_choice = [
        ('ch', 'CHARACTER'),
        ('qu', 'QUALITY'),
        ('ex', 'EXAM'),
        ('__', 'LIKE')
    ]
    rating_type = models.CharField(
        max_length=2, choices=rating_type_choice, default='__',
    )
    entity_object = GenericForeignKey('content_type', 'content_id')
    value = models.IntegerField()

    def __str__(self) -> str:
        return self.user.username + ' ' + self.rating_type + ' ' + str(self.value)
