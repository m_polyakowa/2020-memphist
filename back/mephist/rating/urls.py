from django.urls import path

from .views import add_rating, get_rating, rating_detail

urlpatterns = [
    path('add_rating', add_rating),
    path('user/<user_type>/<id>', rating_detail),
    path('<user_type>/<id>', get_rating),
]
