from django.urls import path

from .views import ImageDetail, ImageUpload

urlpatterns = [
    path('upload', ImageUpload.as_view()),
    path('<pk>', ImageDetail.as_view()),
]
