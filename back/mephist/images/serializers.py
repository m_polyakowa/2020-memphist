import filetype

from django.core.files.uploadedfile import InMemoryUploadedFile
from rest_framework import serializers

from .models import Image


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = '__all__'

    def validate_image(self, value: InMemoryUploadedFile) -> InMemoryUploadedFile:
        kind = filetype.guess(value.read(300))
        if kind and kind.extension.lower() in Image.ALLOWED_EXTENSIONS:
            value.seek(0)
            return value
        raise serializers.ValidationError(
            'Image type is not supported. Supported types are: ' + ', '.join(Image.ALLOWED_EXTENSIONS)
        )
