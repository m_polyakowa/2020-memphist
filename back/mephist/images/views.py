from django.core.handlers.wsgi import WSGIRequest
from django.shortcuts import get_object_or_404
from mephist.utils import calculate_sha256
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN
from rest_framework.views import APIView

from .models import Image
from .serializers import ImageSerializer


class ImageUpload(APIView):
    parser_classes = (MultiPartParser, FormParser)
    permission_classes = [IsAdminUser]

    def post(self, request: WSGIRequest) -> Response:
        file_hash = calculate_sha256(request.FILES['image'])
        try:
            image = Image.objects.get(file_hash=file_hash)
            serialized = ImageSerializer(image, context={'request': request})
            return Response(serialized.data)
        except Image.DoesNotExist:
            request.data['file_hash'] = file_hash

        serialized = ImageSerializer(data=request.data, context={'request': request})
        if serialized.is_valid():
            serialized.save()
            return Response(serialized.data, status=HTTP_201_CREATED)
        else:
            return Response(serialized.errors, status=HTTP_400_BAD_REQUEST)


class ImageDetail(APIView):

    def get(self, request: WSGIRequest, pk: int) -> Response:
        image = get_object_or_404(Image, pk=pk)
        serialized = ImageSerializer(image, context={'request': request})
        return Response(serialized.data)

    def delete(self, request: WSGIRequest, pk: int) -> Response:
        if not request.user.is_staff:
            return Response(status=HTTP_403_FORBIDDEN)
        subject_artifact = get_object_or_404(Image, pk=pk)
        subject_artifact.delete()
        return Response(status=HTTP_204_NO_CONTENT)
