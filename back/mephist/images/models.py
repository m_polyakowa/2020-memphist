from django.db import models


def destination(instance: 'Image', filename: str) -> str:
    return instance.file_hash


class Image(models.Model):
    ALLOWED_EXTENSIONS = ['jpeg', 'png', 'bmp', 'gif', 'ico']

    class Meta:
        db_table = 'Image'
        verbose_name = 'Image'

    image = models.FileField(upload_to=destination)
    date_created = models.DateTimeField(verbose_name='Дата загрузки', auto_now_add=True, unique=True)
    file_hash = models.CharField(verbose_name='sha256 хеш', max_length=64, unique=True)

    def delete(self, *args: tuple, **kwargs: dict) -> tuple:
        self.image.delete()
        super().delete(*args, **kwargs)
