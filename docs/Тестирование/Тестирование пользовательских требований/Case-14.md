#### Тест-кейс №14.1 Просмотр списка учебных дисциплин преподавателя для неавторизованного пользователя
###### Предусловие: - 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Преподаватель";
4. Выбрать конкретного преподавателя из списка;
5. Пролистать вниз, после "Дисциплины, которые ведет преподаватель" будет отображен список.
###### Ожидаемый результат: отображен список учебных дисциплин

#### Тест-кейс №14.2 Просмотр списка учебных дисциплин преподавателя для авторизованного пользователя
###### Предусловие: Авторизация
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Преподаватель";
4. Выбрать конкретного преподавателя из списка;
5. Пролистать вниз, после "Дисциплины, которые ведет преподаватель" будет отображен список.
###### Ожидаемый результат: отображен список учебных дисциплин
