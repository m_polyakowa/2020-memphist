#### Тест-кейс №9.1 Предоставление возможности постать лайк/дизлайк к посту развлекательного контента авторизованному пользователю
###### Предусловие: Авторизация 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Развлекательный контент";
4. Выбрать заголовок;
5. Нажать на заголовок;
6. Пролистать в конец содержания;
7. Поставить лайк/дизлайк.

###### Ожидаемый результат: Отображение добавленного лайка/дизлайка
