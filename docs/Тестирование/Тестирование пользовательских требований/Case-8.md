#### Тест-кейс №8.1 Отображение списка заголовков развлекательного контента авторизованному пользователю
###### Предусловие: Авторизация 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Развлекательный контент".

###### Ожидаемый результат: Отображение списка заголовков развлекательного контента

#### Тест-кейс №8.2 Отображение списка заголовков развлекательного контента неавторизованному пользователю
###### Предусловие: - 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Развлекательный контент";

###### Ожидаемый результат: Отображение списка заголовков развлекательного контента

#### Тест-кейс №8.3 Выбор поста через заголовок развлекательного контента авторизованному пользователю
###### Предусловие: Авторизация 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Развлекательный контент";
4. Выбрать заголовок;
5. Нажать на заголовок.

###### Ожидаемый результат: Вывод содержимого поста развлекательного контента

#### Тест-кейс №8.4 Выбор поста через заголовок развлекательного контента неавторизованному пользователю
###### Предусловие: - 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Развлекательный контент";
4. Выбрать заголовок;
5. Нажать на заголовок.

###### Ожидаемый результат: Вывод содержимого поста развлекательного контента

#### Тест-кейс №8.5 Отображение поста развлекательного контента авторизованному пользователю
###### Предусловие: Авторизация 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Развлекательный контент";
4. Выбрать заголовок;
5. Нажать на заголовок;
6. Просмотреть содержание.

###### Ожидаемый результат: Возможность просмотреть содержимое поста развлекательного контента

#### Тест-кейс №8.6 Отображение поста развлекательного контента неавторизованному пользователю
###### Предусловие: - 
1. Зайти на сайт;
2. Перейти в меню слева;
3. Открыть раздел "Развлекательный контент";
4. Выбрать заголовок;
5. Нажать на заголовок;
6. Просмотреть содержание.

###### Ожидаемый результат: Возможность просмотреть содержимое поста развлекательного контента
